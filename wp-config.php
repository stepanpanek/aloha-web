<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'aloha' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '(r}S^!S53EP$+eQBh>:c#qU8YJW:f~.XD<)D+H!^/<s1%]`}g])8d&[SugKl;%0j' );
define( 'SECURE_AUTH_KEY',  '9!cwt!b43.C%rRoR`evy[<Tc!nrwf$6;:*gHml%_.f$(4F7*bu35Csv!zu7#2<Ct' );
define( 'LOGGED_IN_KEY',    'Xqts;<ZJp-&}HBfBqhR!1hl3=G}+ARepHHSv!/>4#[t^f9V5.<f~|iJmu0y$rw7t' );
define( 'NONCE_KEY',        '[$|oE4Hy=3d7t,{#.9?7+m/Ah=R95HLn|XFf~I!un,[X>s|jM)/f&rg<[XHObA;I' );
define( 'AUTH_SALT',        '6ivDt-5cHGo C?kD8~jp8P~.-R5~AKW2HDEvz_34>^{v/;sl?P9L}U9qe82Lm)0)' );
define( 'SECURE_AUTH_SALT', 'EE7V;D~CC4:u&*#L~P+dJIX+-U82<s~F~1ohZJ=vIP[ubQMG;dos e<klB8Epxgx' );
define( 'LOGGED_IN_SALT',   'ba+E~TULMMOsnh-Qt!@Mv a5%.v,Zjx[Jw# C-n9G7-I7k@$bnh`i `wrP[)}8eR' );
define( 'NONCE_SALT',       'ri-!m!{5,$ks)@Hy^|mF:5#yk?+=C>,C~DqJbj%Wy8;ZN}>KJX.Oo`-tw(}$vj8@' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wxp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
