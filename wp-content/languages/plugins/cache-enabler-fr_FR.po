# Translation of Plugins - Cache Enabler &#8211; WordPress Cache - Development (trunk) in French (France)
# This file is distributed under the same license as the Plugins - Cache Enabler &#8211; WordPress Cache - Development (trunk) package.
msgid ""
msgstr ""
"PO-Revision-Date: 2016-05-20 06:57:08+0000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: GlotPress/3.0.0-alpha.2\n"
"Language: fr\n"
"Project-Id-Version: Plugins - Cache Enabler &#8211; WordPress Cache - Development (trunk)\n"

#: inc/cache_enabler.class.php:740
msgid "Empty"
msgstr "Vide"

#: inc/cache_enabler.class.php:1809
msgid "Completely"
msgstr "Complètement"

#: inc/cache_enabler.class.php:1808
msgid "Page specific"
msgstr "Page spécifique"

#: inc/cache_enabler.class.php:1841
msgid "Edit"
msgstr "Modifier"

#: inc/cache_enabler.class.php:1843
msgid "OK"
msgstr "OK"

#: inc/cache_enabler.class.php:1844
msgid "Cancel"
msgstr "Annuler"

#: inc/cache_enabler.class.php:741
msgid "Cache Size"
msgstr "Taille du cache"

#. Author of the plugin
msgid "KeyCDN"
msgstr "KeyCDN"

#. Author URI of the plugin
msgid "https://www.keycdn.com"
msgstr "https://www.keycdn.com"

#. Plugin Name of the plugin
msgid "Cache Enabler"
msgstr "Cache Enabler"

#: inc/cache_enabler.class.php:1920
msgid "HTML"
msgstr "HTML"

#: inc/cache_enabler.class.php:1919
msgid "Disabled"
msgstr "Désactivé"

#: inc/cache_enabler.class.php:675
msgid "Settings"
msgstr "Réglages"

#: inc/cache_enabler.class.php:991
msgid "The cache has been cleared."
msgstr "Le cache a été effacé."

#: inc/cache_enabler.class.php:739
msgid "Disk Cache"
msgstr "Cache du disque"

#: inc/cache_enabler.class.php:818
msgid "Clear Cache"
msgstr "Vider le cache"

#: inc/cache_enabler.class.php:1839
msgid "Clear cache"
msgstr "Vider le cache"

#: inc/cache_enabler.class.php:2212
msgid "Cache Expiry"
msgstr "Expiration du cache"

#: inc/cache_enabler.class.php:2221
msgid "Cache Behavior"
msgstr "Comportement du cache"

#: inc/cache_enabler.class.php:2259
msgid "Pre-compression of cached pages. Needs to be disabled if the decoding fails in the web browser."
msgstr "Pré-compression des pages mises en cache. Doit être désactivé si le décodage échoue dans le navigateur web."

#: inc/cache_enabler.class.php:2274
msgid "Cache Exclusions"
msgstr "Exclusions du cache"

#: inc/cache_enabler.class.php:1980 inc/cache_enabler.class.php:2323
msgid "Cache Minification"
msgstr "Minification du cache"

#: inc/cache_enabler.class.php:1989 inc/cache_enabler.class.php:2192
msgid "Cache Enabler Settings"
msgstr "Réglages de Cache Enabler"

#. Description of the plugin
msgid "Simple and fast WordPress disk caching plugin."
msgstr "Une extension simple et rapide pour la mise en cache du disque de WordPress."

#: inc/cache_enabler.class.php:848 inc/cache_enabler.class.php:850
msgid "Clear URL Cache"
msgstr "Vider le cache de l’URL"

#: inc/cache_enabler.class.php:2242
msgid "Clear the complete cache if a new comment has been posted (instead of only the page specific cache)."
msgstr "Vider le cache complètement si un nouveau commentaire est publié (au lieu du cache spécifique de la page)."