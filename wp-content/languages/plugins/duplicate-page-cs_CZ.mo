��          �      L      �    �  �   �  &   �  V   �     A  X   I     �     �     �     �     �     �  �     e   �  ?   �     >     G     Y  K  r  �   �  �   �  *   �	  �   �	  
   t
  T   
     �
     �
     �
     
     )     I  d   Y  l   �  C   +  
   o     z     �     
                                                 	                                              <div class="error settings-error notice is-dismissible" id="setting-error-settings_updated"> 
<p><strong>Settings not saved.</strong></p><button class="notice-dismiss" type="button"><span class="screen-reader-text">Dismiss this notice.</span></button></div> <div class="updated settings-error notice is-dismissible" id="setting-error-settings_updated"> 
<p><strong>Settings saved.</strong></p><button class="notice-dismiss" type="button"><span class="screen-reader-text">Dismiss this notice.</span></button></div> <strong>Saving Please wait...</strong> Add a suffix for duplicate or clone post as Copy, Clone etc. It will show after title. Buy PRO Contribute some donation, to make plugin more stable. You can pay amount of your choice. Donate Duplicate Page Duplicate Page Settings  Duplicate Post Status Duplicate Post Suffix Duplicate This Please select any post redirection, redirect you to selected after click on duplicate this link. <strong>Default:</strong> To current list. Please select any post status you want to assign for duplicate post. <strong>Default:</strong> Draft. Redirect to after click on <strong>Duplicate This Link</strong> Settings To All Posts List To Duplicate Edit Screen PO-Revision-Date: 2018-02-09 07:19:51+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n == 1) ? 0 : ((n >= 2 && n <= 4) ? 1 : 2);
X-Generator: GlotPress/3.0.0-alpha.2
Language: cs_CZ
Project-Id-Version: Plugins - Duplicate Page - Development (trunk)
 <div class="error settings-error notice is-dismissible" id="setting-error-settings_updated"> 
<p><strong>Nastavení nebylo uloženo!</strong></p><button class="notice-dismiss" type="button"><span class="screen-reader-text">Rozumím.</span></button></div> <div class="updated settings-error notice is-dismissible" id="setting-error-settings_updated"> 
<p><strong>Nastavení bylo uloženo.</strong></p><button class="notice-dismiss" type="button"><span class="screen-reader-text">Rozumím.</span></button></div> <strong>Ukládá se nastavení...</strong> Zde si můžete zvolit příponu, kterou plugin připojí k názvu duplikované položky. Může zůstat prázdné, nebo například: kopie. Koupit PRO Pokud je pro vás tento plugin užitečný, můžete vývojářovi třeba přispět. Darovat Duplikovat stránku Nastavení Duplicate Page Stav duplikátu po vytvoření Přípona duplikovaného obsahu Duplikovat toto Co se stane po kliknutí na tlačítko duplikovat. <strong>Výchozí:</strong> Zůstat kde jste byli Když se vytvoří duplikát, je potřeba mu nastavit stav publikovaní. <strong>Výchozí:</strong> Koncept Kam přesměrovat potom, co se vytvoří <strong>duplikát</strong> Nastavení Seznam všech příspěvků Otevřít duplikát pro úpravu 