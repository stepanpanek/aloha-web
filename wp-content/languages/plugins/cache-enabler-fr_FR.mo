��          �   %   �      `     a     p     ~     �     �     �  
   �     �     �     �     �  d      
   e     p  
   y     �     �     �     �     �     �  _   �       .        D     `  9  w     �     �     �     �               .     >     F     U     o  m   ~     �     �                    $     )     0     3  q   D  	   �  L   �          '                            	                                                                                  
              Cache Behavior Cache Enabler Cache Enabler Settings Cache Exclusions Cache Expiry Cache Minification Cache Size Cancel Clear Cache Clear URL Cache Clear cache Clear the complete cache if a new comment has been posted (instead of only the page specific cache). Completely Disabled Disk Cache Edit Empty HTML KeyCDN OK Page specific Pre-compression of cached pages. Needs to be disabled if the decoding fails in the web browser. Settings Simple and fast WordPress disk caching plugin. The cache has been cleared. https://www.keycdn.com PO-Revision-Date: 2016-05-20 06:57:08+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: fr
Project-Id-Version: Plugins - Cache Enabler &#8211; WordPress Cache - Development (trunk)
 Comportement du cache Cache Enabler Réglages de Cache Enabler Exclusions du cache Expiration du cache Minification du cache Taille du cache Annuler Vider le cache Vider le cache de l’URL Vider le cache Vider le cache complètement si un nouveau commentaire est publié (au lieu du cache spécifique de la page). Complètement Désactivé Cache du disque Modifier Vide HTML KeyCDN OK Page spécifique Pré-compression des pages mises en cache. Doit être désactivé si le décodage échoue dans le navigateur web. Réglages Une extension simple et rapide pour la mise en cache du disque de WordPress. Le cache a été effacé. https://www.keycdn.com 