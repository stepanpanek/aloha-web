﻿<?php /* Template Name: Recipes */ ?>

<?php get_header(); ?>

	<!-- Article -->	
	<article class="article article--recipes">

		<!-- Header -->
		<header class="article__header lazy" data-bg="<?php echo get_the_post_thumbnail_url();?>">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<p class="introText">Objevte</p>
						<h1>Mé recepty</h1>
					</div>
				</div>
			</div>
		</header>
		<!-- / -->
		
		<!-- Content -->
		<div id="content" class="article__content">
			<div class="container">
				<div class="row">
					<div class="col-md-12 article--recipes__sorting">
						<div id="orderBy">
							<p>Seřadit podle:</p>
							<ul>
								<li class="recipes__sorting" attr="1">Dle data přidání sestupně</li>
								<li class="recipes__sorting" attr="2">Dle data přidání vzestupně</li>
								<li class="recipes__sorting" attr="3">Dle názvu od A do Z</li>
								<li class="recipes__sorting" attr="4">Dle názvu od Z do A</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="row">

					<div class="col-lg-4">
					
						<!-- Sidebar -->
                       <form action="<?php echo site_url() ?>/wp-admin/admin-ajax.php" method="POST" id="filter">
						<aside id="sidebar" class="aside aside--sidebar" role="navigation">
							<div class="block block--search">
								<label for="search">
									<input type="search" placeholder="Hledat recept" name="search" id="search">
									<a href="#" class="btn" id="searchRecipe">Vyhledat</a>
								</label>
							</div>
							<div class="block block--category">
								<div class="block__header">
									<h2>Kategorie</h2>
								</div>
								<div class="block__body">
									<label for="">
										<span>Test</span>
										<input type="checkbox" name="category" value="test" class="category__filter">
										<div></div>
									</label>
									<label for="">
										<span>Test</span>
										<input type="checkbox" name="category" value="test" class="category__filter">
										<div></div>
									</label>
									<label for="">
										<span>Test</span>
										<input type="checkbox" name="category" value="test" class="category__filter">
										<div></div>
									</label>
									<label for="">
										<span>Test</span>
										<input type="checkbox" name="category" value="test" class="category__filter">
										<div></div>
									</label>
									<label for="">
										<span>Test</span>
										<input type="checkbox" name="category" value="test" class="category__filter">
										<div></div>
									</label>
								</div>
							</div>
							<div class="block block--reset">
								<div id="resetFilter">
									<img class="lazy" data-src="<?php echo get_template_directory_uri(); ?>/img/icon-close.svg" alt="Zrušit filtr">
									<span>Zrušit filtr</span>
								</div>
							</div>
						</aside>
                        <input type="hidden" name="action" value="myfilter">
                        <input type="hidden" name="paged" id="paged">
                        <input type="hidden" name="sorted" id="sorted">
                        </form>
						<!-- / -->

					</div>

					<div class="col-lg-8">

						<div class="boxes boxes--start">

							<?php $catquery = new WP_Query( 'post_type=recepty&posts_per_page=6' ); ?>

							<?php while($catquery->have_posts()) : $catquery->the_post(); ?>
	
								<div class="box box--2">
									<a class="box__link" href="<?php echo get_permalink();?>"></a>
									<div class="box__header">
										<?php the_post_thumbnail();?>
									</div>
									<div class="box__body">
										<h3><?php echo get_the_title();?></h3>
									</div>
								</div>
								<?php wp_reset_postdata(); ?>
							<?php endwhile; ?>
                        <div class = "pagination"><?php
                        $page = $_POST['paged'];
                        $paged = ( $page ) ? $page : 1;
                        echo paginate_links( array(
                                'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                                'total'        => $catquery->max_num_pages,
                                'current'      => max( 1, $page ),
                                'format'       => '?paged=%#%',
                                'show_all'     => false,
                                'type'         => 'plain',
                        
                            ) );
                        ?>
                        </div>
						</div>

					</div>

				</div>
			</div>
		</div>
		<!-- / -->

	</article>
	<!-- / -->

<?php get_footer(); ?>