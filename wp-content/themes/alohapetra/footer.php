   	</main>
	<!-- / -->
	
	<!-- -->
	<aside id="contacts" class="aside aside--preFooter" role="complementary">
		<div class="container">
            <p class="introText">Kontaktujte mě</p>
			<div class="row">
				<div class="col-md-12">
					<h2>Zavolejte mi nebo napište. <br /> Vzdálenost nehraje roli, spolupracuji <br /> s lidmi osobně i na dálku.</h2>
					<ul>
						<li><a href="tel:+420732266999"><img class="lazy" data-src="<?php echo get_template_directory_uri();?>/img/icon-phone.svg" alt="Ikona telefonu"><span>+420 732 266 999</span></a></li>
						<li><a href="mailto:info@alohapetra.cz"><img class="lazy" data-src="<?php echo get_template_directory_uri();?>/img/icon-email.svg" alt="Ikona e-mailu"><span>info@alohapetra.cz</span></a></li>
					</ul>
					<p>Ráda vám zodpovím všechny vaše dotazy.</p>
					<div class="btn-wrapper">
						<a class="btn btn--primary" href="mailto:info@alohapetra.cz"><span>Napsat zprávu</span></a>
					</div>
					<ul>
						<li><a href="https://www.facebook.com/alohapetra.cz" target="_blank"><img class="lazy" data-src="<?php echo get_template_directory_uri();?>/img/icon-fb.svg" alt="Ikona FB"></a></li>
						<li><a href="https://www.instagram.com/alohapetra.cz/" target="_blank"><img class="lazy" data-src="<?php echo get_template_directory_uri();?>/img/icon-ig.svg" alt="Ikona IG"></a></li>
					</ul>
				</div>
			</div>
		</div>
	</aside>
	<!-- / -->

    <!-- Site footer -->
    <footer class="footer footer--main" role="contentinfo">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<a id="logoFooter" href="/">
						<img class="lazy" data-src="<?php echo get_template_directory_uri();?>/img/logo.svg">
					</a>
				</div>
				<div class="col-md-9 boxes">
					<ul>
						<li>&copy <?php echo get_the_date('Y'); ?> Aloha Petra</li>
						<li><a href="/ochrana-osobnich-udaju">Ochrana osobních údajů</a></li>
						<li><a href="/obchodni-podminky">Obchodní podmínky</a></li>
						<li><a href="https://gradie.cz" target="_blank">Vytvořilo Gradie</a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>
	<!-- / -->

	<!-- Modals --
	<div class="modals">

		<div id="demand" class="modal modal--demand">
			<div class="modal__wrapper">
				<header class="modal__header">
					<div class="modal__close">
						<span><img src="<?php echo get_template_directory_uri(); ?>/img/close.svg" alt="Křížek"></span>
					</div>
					<img class="modal__logo" src="<?php echo get_template_directory_uri(); ?>/img/logo-footer.svg" alt="Logo Zoom cargo">
					<h2>Poptávka přepravy</h2>
					<img class="modal__nonstop" src="<?php echo get_template_directory_uri(); ?>/img/icon-clock-nonstop-long.svg" alt="Logo Zoom cargo">
				</header>
				<div class="modal__body">
					<p>Rádi Vám sdělíme více o našich přepravních řešeních a cenách. Abychom našli efektivní řešení Vašeho požadavku na přepravu, sdělte nám prosím základní údaje o Vaší zásilce s kontaktem na Vás. Obsáhlejší poptávky nebo poptávky s přílohami pošlete prosím prostřednictvím e-mailu na <a href="mailto:info@zoom-cargo.com">info@zoom-cargo.com</a></p>
					<div class="divider"></div>
					<?php echo do_shortcode('[contact-form-7 id="87" title="Poptavka"]');?>
				</div>
			</div>
			<div class="modal__overlay"></div>
		</div>

	</div>
	<!-- / -->
	
<?php wp_footer(); ?>

</body>
</html>