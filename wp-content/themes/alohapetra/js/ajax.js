(function($){   

   $('#resetFilter').click(function(e){
        $('#paged').val('');
        resetForm($('#filter'));
        window.location.reload();
        //$.fn.filterRecipe();
        
   });
   
	$('.category__filter').on('change',function(){
         $.fn.filterRecipe();
	});
    
 $.fn.filterRecipe = function(){
		var filter = $('#filter');
		$.ajax({
			url:filter.attr('action'),
			data:filter.serialize(), // form data
			type:filter.attr('method'), // POST
			beforeSend:function(xhr){
				//filter.find('button').text('Hled�m...'); // changing the button label
			},
			success:function(data){
				//filter.find('button').text('Filtrov�no'); // changing the button label back
				$('.boxes--start').html(data); // insert data
			}
		});
		return false;
}

	$('.page-numbers').on('click',function(){
        var pageNum = '';
        var strana = 0;
        pageNum  = $(this).attr('href').match(/\d+/)[0];    
        $('#paged').val(pageNum);
        $.fn.filterRecipe();
        return false;
    }); 

})(jQuery);

function resetForm($form) {
    $form.find('input:text, input:password, input:file, select, textarea').val('');
    $form.find('input:radio, input:checkbox')
         .removeAttr('checked').removeAttr('selected');
}