(function($){   

/* ---------------------------------------------------------------------
  Core - functions
----------------------------------------------------------------------*/

  var $win = $(window);

  // Scroll to anchor
  $(document).ready(function() {
  // Add smooth scrolling to all links
    $("a").on('click', function(event) {

      // Make sure this.hash has a value before overriding default behavior
      if (this.hash !== "") {

        // Store hash
        var hash = this.hash;

        // Using jQuery's animate() method to add smooth page scroll
        // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
        $('html, body').animate({
          scrollTop: $(hash).offset().top+0-20
        }, 2000, function() {

          // Add hash (#) to URL when done scrolling (default click behavior)
          window.location.hash = hash;
        });
        return false;
      }
    });
  });

// Initialize Lazyload
var lazyLoadInstance = new LazyLoad({
  treshold: 700,
});

// Initialize Swiper
var swiper = new Swiper('.swiper-container-recipe', {
speed: 750,
breakpoints: {
  660: {
    slidesPerView: 1,
    spaceBetween: 0,
  },
  768: {
    slidesPerView: 2,
    spaceBetween: 18,
  },
  1980: {
    slidesPerView: 3,
    spaceBetween: 18,
  },
},
pagination: {
  el: '.swiper-pagination',
  clickable: true,
},
navigation: {
  nextEl: '.swiper-nav .next',
  prevEl: '.swiper-nav .prev',
},
});

// Initialize Swiper
var swiper = new Swiper('.swiper-container-hp', {
  speed: 750,
  slidesPerView: 1,
  noSwiping: true,
  allowTouchMove: false,
  navigation: {
    nextEl: '.swiper-nav .next',
    prevEl: '.swiper-nav .prev',
  },
});

/*
  // Show more text
  $('.accordion__btn').click(function(e) {
    e.preventDefault();
    var el=$(this);
    var accordionContent=el.prev(".accordion__content");
    var fullHeight=accordionContent[0].scrollHeight;
    var shortHeight=accordionContent.height();

    accordionContent.css("height",fullHeight+"px"); 
    $(this).html('Zpět');

  });
*/

// 
$('.article--priceList__single .row').click(function() {
  $(this).toggleClass('is-active');
  $(this).find('ul').slideToggle();
});

// 
$('.block__header').click(function() {
  $(this).toggleClass('is-active');
  $(this).siblings('.block__body').slideToggle();
});

// Print function on btn
$('#print').click(function(){
  window.print();
});

// Chocolat gallery
$(document).ready(function(){
  $('.chocolat-parent').Chocolat();
});

/* ---------------------------------------------------------------------
  Header - functions
----------------------------------------------------------------------*/

  // Responsive menu
  $('#toggle').on('click', function(){
    $(this).toggleClass('is-active');
    $('#mainMenu').toggleClass('is-active');
    $('html').toggleClass('is-active');
    $('body').toggleClass('is-active');
  });

  // Close menu overlay on action
  if ($win.width() <= 992) {
    
    $('#mainMenu li:not(.menu-item-has-children) a').on('click', function(){
      $('#toggle.is-active').click();
    });

    $('#mainMenu li.menu-item-has-children').on('click', function(){
      $(this).find('.sub-menu').slideToggle();
    });
  }
  
  $(window).scroll(function() {    
    var scroll = $(window).scrollTop();

    if (scroll >= 75) {
        $(".header--main").addClass("is-fixed");
    } else {
        $(".header--main").removeClass("is-fixed");
    }
});

/* ---------------------------------------------------------------------
  Carousel, Gallery - functions
----------------------------------------------------------------------*/

  $("#backToTop").click(function() {
    $("html, body").animate({scrollTop: 0}, 1500);
  });
  
  $("#scrollDown").click(function() {
    var top = $('#scrollDown').offset().top;
    $("html, body").animate({scrollTop: top + 100}, 1200);
  });

  $(".accordion h3").click(function() {
    $(this).parent().toggleClass('is-active');
    $(this).siblings('p').slideToggle();
  });

  $('.btn--booking').on('click', function(){
    $('#bookingBasic').fadeIn();
    $('body').addClass('is-active');
  });

  $('.btn--booking--1').on('click', function(){
    $('#booking1').fadeIn();
    $('body').addClass('is-active');
  });

  $('.btn--booking--2').on('click', function(){
    $('#booking2').fadeIn();
    $('body').addClass('is-active');
  });

  $('.btn--booking--3').on('click', function(){
    $('#booking3').fadeIn();
    $('body').addClass('is-active');
  });
  
  $('.btn--booking--4').on('click', function(){
    $('#booking4').fadeIn();
    $('body').addClass('is-active');
  });
  
  $('#sw').on('click', function(){
    $('.modal--sw').fadeIn();
    $('body').addClass('is-active');
  });

  $('.modal__overlay').on('click', function(){
    $(this).parent().fadeOut();
    $('body').removeClass('is-active');
  });

/* ---------------------------------------------------------------------
  Sections - functions
----------------------------------------------------------------------*

  // Overview boxes
  $("#services .box").each(function(){
      var serv = $(this);

    // Service box
    var services = new TimelineMax();
    services.fromTo(serv, .75, {autoAlpha: 0, y: 30, ease: Power1.ease}, {autoAlpha: 1, y: 0, ease: Power1.ease})
    
    var scene = new ScrollMagic.Scene({
      triggerElement: this,
      offset: -50,
      reverse: !1
    })
    .setTween(services).addTo(controller); 
  });

  // Map
  var map = new TimelineMax();
  map.staggerFromTo(".section--contacts__map", .75, {autoAlpha: 0, y: 30, ease: Power1.ease}, {autoAlpha: 1, y: 0, ease: Power1.ease})
  var scene = new ScrollMagic.Scene({
    triggerElement: '.section--contacts__map',
    offset: -50,
    reverse: !1
  })
  .setTween(map).addTo(controller); 

  // Form
  var form = new TimelineMax();
  form.staggerFromTo(".wpcf7", .75, {autoAlpha: 0, y: 30, ease: Power1.ease}, {autoAlpha: 1, y: 0,ease: Power1.ease})
  var scene = new ScrollMagic.Scene({
    triggerElement: '.wpcf7',
    offset: -50,
    reverse: !1
  })
  .setTween(form).addTo(controller); 

  // Intro
  var intro = new TimelineMax();
  intro.staggerFromTo(".section--intro li", .75, {autoAlpha: 0, y: 30, ease: Power1.ease}, {autoAlpha: 1, y: 0, ease: Power1.ease}, .225)
  var intro = new ScrollMagic.Scene({
    triggerElement: '.section--intro',
    offset: -50,
    reverse: !1
  })
  .setTween(intro).addTo(controller); 

  // Specialization
  var boxes = new TimelineMax();
  boxes.staggerFromTo(".section--specialization .box", .75, {autoAlpha: 0, y: 30, ease: Power1.ease}, {autoAlpha: 1, y: 0, ease: Power1.ease}, .225)
  var boxes = new ScrollMagic.Scene({
    triggerElement: '.section--specialization .boxes',
    offset: -50,
    reverse: !1
  })
  .setTween(boxes).addTo(controller); 

  // Overview boxes
  $(".section--servicesOverview .box").each(function(){
      var bHeader = $(this).find(".box__header");
      var bBody = $(this).find(".box__body");

    // Service box
    var serviceBox = new TimelineMax();

    serviceBox
        .fromTo(bHeader, 1, {autoAlpha: 0, y: 60, ease: Power3.ease}, {autoAlpha: 1, y: 0})
        .fromTo(bBody, 1, {autoAlpha: 0, y: 60}, {autoAlpha: 1, y: 0, ease: Power2.ease}, "-=.825")

      var serviceBox = new ScrollMagic.Scene({
      triggerElement: this,
      offset: -100,
      reverse: !1
    })
    .setTween(serviceBox).addTo(controller);
  });

  // Overview boxes
  $(".section--pricelist .pricelist__row").each(function(){
      var pRow = $(this);

    // Service box
    var pricelist = new TimelineMax();

    pricelist
        .fromTo(pRow, 1, {autoAlpha: 0, y: 30, ease: Power3.ease}, {autoAlpha: 1, y: 0})
      var pricelist = new ScrollMagic.Scene({
      triggerElement: this,
      offset: -150,
      reverse: !1
    })
    .setTween(pricelist).addTo(controller);
  });

  // Overview boxes
  $(".section--pricelist .additional").each(function(){
      var adit = $(this);

    // Additional 
    var additional = new TimelineMax();

    additional
        .fromTo(adit, 1, {autoAlpha: 0, y: 30, ease: Power3.ease}, {autoAlpha: 1, y: 0})
      var additional = new ScrollMagic.Scene({
      triggerElement: this,
      offset: -150,
      reverse: !1
    })
    .setTween(additional).addTo(controller);
  });


/* ---------------------------------------------------------------------
  Ornaments
  
  
  // Footer
  var footer = new TimelineMax();
  footer.staggerFromTo("#services .boxes", 1, {autoAlpha: 0, y: 30, ease: Power1.ease}, {autoAlpha: 1, y: 0, ease: Power1.ease}, .5)
        .staggerFromTo("#services .social li", 1, {autoAlpha: 0, y: 30, ease: Power1.ease}, {autoAlpha: 1, y: 0, ease: Power1.ease}, .15, "-=1")
        .fromTo("#services .footer--main__bottom", 1, {autoAlpha: 0, ease: Power1.ease}, {autoAlpha: 1, delay: -.25, ease: Power1.ease})
  var scene = new ScrollMagic.Scene({
    triggerElement: '#services',
    offset: -150,
    reverse: !1
  })
  .setTween(footer).addTo(controller); 

----------------------------------------------------------------------

// Left 
  var leftOrnament = new TimelineMax ()
      .add([
          TweenMax.fromTo(".ornament--left", .5, {top: "-1%"}, {top: "7%", ease: Power2.ease})
      ]);
  var sceneLeftOrnament = new ScrollMagic.Scene({triggerElement: ".section--bg"})
  .setTween(leftOrnament)
  .addTo(controller);

  */
  var pocet = 0;
  $('#searchRecipe').click(function(){
    $.fn.filterRecipe();
    return false;
  });
  
  
   $('#search').keydown(function(event){
      if(event.keyCode == 13) {
        event.preventDefault();
        $.fn.filterRecipe();
        return false;
      }
    });
  
   $('.recipes__sorting').click(function(e){
      var sort = $(this).attr('attr');
      $('#sorted').val(sort);
      $.fn.filterRecipe();         
   });

   $('#resetFilter').click(function(e){
        $('#paged').val('');
        resetForm($('#filter'));
        window.location.reload();
        
   });
   
	$('.category__filter').on('change',function(){
 
         $('#paged').val('');
         $.fn.filterRecipe();
	});
     
 $.fn.filterRecipe = function(){

		var filter = $('#filter');
		$.ajax({
			url:filter.attr('action'),
			data:filter.serialize(), // form data
			type:filter.attr('method'), // POST
			beforeSend:function(xhr){
				filter.find('button').text('Hledám...'); // changing the button label
			},
			success:function(data){
				filter.find('button').text('Filtrováno'); // changing the button label back
                //ajax = 1;
 				$('.boxes--start').html(data); // insert data
			}
		});
		return false;
}

  $('.page-numbers').on('click',function(){
    var pageNum = '';
    var strana = 0;
    pageNum  = $(this).attr('href').match(/\d+/)[0];    
    $('#paged').val(pageNum);
    $.fn.filterRecipe();
    $("html, body").animate({ scrollTop: 0 }, 600);
    return false;
  }); 

  //SELECT OPTIONS AND HIDE OPTION AFTER SELECTION
  $("#orderBy ul li").click(function() {
    $(this).siblings().removeClass('is-selected');
    $(this).addClass('is-selected');
    var text = $(this).delay(500).html();
    $(this).parent('ul').siblings('p').find('span').html(text);
  }); 
    

})(jQuery);

function resetForm($form) {
    $form.find('input:text, input:password, input:file, select, textarea').val('');
    $form.find('input:radio, input:checkbox')
         .removeAttr('checked').removeAttr('selected');
}