<?php

/**
 * Sidebars & Widgets
 */

// enable shortcodes in widgets
add_filter( 'widget_text', 'do_shortcode' );

// Widget area
function my_custom_sidebar() {
    register_sidebar(
        array (
            'name' => __( 'Menu v patičce' ),
            'id' => 'widget2',
			'description' => __( 'Zde se bude naházet menu pro patičku webu.' ),
			'before_widget' => '<div id="fastContact" class="offset-md-1 col-md-3 col-lg-3">',
			'after_widget' => '</div>',
            'before_title' => '<h5 class="aside--widgetArea__title">',
            'after_title' => '</h5>',
        )
	);
	
	register_sidebar(
        array (
            'name' => __( 'Adresa firmy' ),
            'id' => 'widget3',
			'description' => __( 'Zde se bude nacházet adresa firmy.' ),
			'before_widget' => '<div id="companyAddress" class="col-md-3 col-lg-3">',
			'after_widget' => '</div>',
            'before_title' => '<h5 class="aside--widgetArea__title">',
            'after_title' => '</h5>',
        )
	);
	
	register_sidebar(
        array (
            'name' => __( 'Rychlý kontakt' ),
            'id' => 'widget4',
			'description' => __( 'Zde se budou nacházet údaje pro rychlý kontakt.' ),
			'before_widget' => '<div id="footerMenu" class="col-md-3 col-lg-3">',
			'after_widget' => '</div>',
            'before_title' => '<h5 class="aside--widgetArea__title">',
            'after_title' => '</h5>',
        )
	);
		
	register_sidebar(
        array (
            'name' => __( 'Sidebar v článcích' ),
            'id' => 'sidebar',
			'description' => __( 'Přidejte libovolné widgety, které se budou zobrazovat na pravé straně v detailu článku.' ),
        )
    );
}
add_action( 'widgets_init', 'my_custom_sidebar' );