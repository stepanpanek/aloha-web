<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>

	<!-- Header -->
	<header class="article__header">
		
		<div class="article__header__intro">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<a href="<?php echo get_site_url();?>/udalosti" id="back">
							<img class="lazy" data-src="<?php echo get_template_directory_uri();?>/img/arrow-left.svg" alt="Šipka zpět">
							<span>Zpět na události</span>
						</a>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<p class="introText"><?php the_field('product__date');?> / <?php the_field('product__city');?></p>
						<h1><?php the_title(); ?></h1>
					</div>
				</div>
			</div>
		</div>

		<div class="article__header__img">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<img src="<?php echo wp_get_attachment_url( $product->get_image_id() ); ?>" />
					</div>
				</div>
			</div>
		</div>

	</header>
	<!-- / -->
	      
	<!-- Content -->
	<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>
		<div class="container article__content">
			<div class="row">
				<div class="col-md-12">

					<!-- Info -->
					<div class="article--singleProduct__info block">
						<h2 class="introText">Úvod</h2>
						<?php the_field('product__intro'); ?>
					</div>
					<!-- / -->

					<!-- Process -->
					<div class="article--singleProduct__process block">
						<h2 class="introText">Obsah semináře</h2>

						<?php if( have_rows('product__rep') ): ?>
						<ul>
						<?php while( have_rows('product__rep') ): the_row(); 

							// vars
							$processText = get_sub_field('product__rep__li');

							?>

							<li><?php echo $processText; ?></li>

						<?php endwhile; ?>
						</ul>
						<?php endif; ?>
						
					</div>
					<!-- / -->

					<!-- Details -->
					<div class="summary entry-summary article--singleProduct__details">
						<?php
						/**
						 * Hook: woocommerce_single_product_summary.
						 *
						 * @hooked woocommerce_template_single_title - 5
						 * @hooked woocommerce_template_single_rating - 10
						 * @hooked woocommerce_template_single_price - 10
						 * @hooked woocommerce_template_single_excerpt - 20
						 * @hooked woocommerce_template_single_add_to_cart - 30
						 * @hooked woocommerce_template_single_meta - 40
						 * @hooked woocommerce_template_single_sharing - 50
						 * @hooked WC_Structured_Data::generate_product_data() - 60
						 */
						do_action( 'woocommerce_single_product_summary' );
						?>
					</div>
					<!-- / -->

				</div>
			</div>
		</div>



		<!-- Fixed CTA -->
		<div class="fixedCTA">
			<a href="<?php echo get_site_url();?>/?add-to-cart=<?php echo $product->get_id();?>">
				<span>Objednat seminář</span>
				<img class="lazy" data-src="<?php echo get_template_directory_uri();?>/img/icon-cart-rounded.svg" alt="Ikona pro košík">
			</a>	
		</div>
		<!-- / -->

	</div>
	<!-- / -->
	<?php do_action( 'woocommerce_after_single_product' ); ?>
