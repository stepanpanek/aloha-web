﻿<?php get_header() ?>

	<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : the_post() ?>

	<!-- Article -->	
	<article class="article article--singleStory">

		<!-- Header -->
		<header class="article__header">
			
			<div class="article__header__intro">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<a href="<?php echo get_site_url();?>/pribehy" id="back">
								<img class="lazy" data-src="<?php echo get_template_directory_uri();?>/img/arrow-left.svg" alt="Šipka zpět">
								<span>Zpět na přehled</span>
							</a>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="img-wrapper">
								<img src="<?php echo get_the_post_thumbnail_url();?>" alt="Fotka">
							</div>
							<h1><?php the_title(); ?></h1>
							<span><?php the_field('testimonials__position') ?></span>
						</div>
					</div>
				</div>
			</div>

		</header>
		<!-- / -->
		
		<!-- Content -->
		<div class="article__content">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		</div>
		<!-- / -->

	</article>
	<!-- / -->

		<?php endwhile ?>
	<?php endif ?>

<?php get_footer() ?>