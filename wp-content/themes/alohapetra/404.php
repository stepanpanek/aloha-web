<?php get_header(); ?>

		<!-- Article -->
		<article class="article article--default article--404">

			<!-- Hello -->
			<section id="hello" class="section section--hello">
				<div class="container container--large">
					<div class="section--hello__bg">
						<div></div>
					</div>
					<span id="scrollDown">
						<img src="<?php echo get_template_directory_uri();?>/img/arrow-up.svg" alt="Šipka nahoru">
					</span>
				</div>
				<div class="section--hello__content">       
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="section--hello__wrapper">
									<h1>Stránka nebyla nalezena</h1>
									<p>Nejspíše jste zadali špatný název, zkuste to znovu, nebo se vraťtě zpět na úvodní stranu.</p>
									<a class="btn btn--primary" href="<?php echo get_site_url(); ?>"><span>Zpět na úvod</span></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- / -->

		</article>
		<!-- / -->

<?php get_footer(); ?>