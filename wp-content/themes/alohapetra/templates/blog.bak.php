﻿<?php /* Template Name: Blog old */ ?>

<?php get_header(); ?>

    <!-- Article -->
    <article class="article article--blog">

        <!-- Header -->
        <header class="article__header lazy" data-bg="<?php echo get_the_post_thumbnail_url();?>">
            
            <!-- Hello -->
            <section id="hello" class="section section--hello">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <h1><?php the_field('hello__title');?></h1>
                            <a class="scrollDown" href="#content">
                                <img class="lazy" data-src="<?php echo get_template_directory_uri();?>/img/arrow-down.svg" alt="Šipka dolů">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="section--hello__bg">
                    <img class="lazy" data-src="<?php the_field('hello__img');?>" alt="Aloha Petra úvodní obrázek">
                </div>
            </section>
            <!-- / -->

        </header>
        <!-- / -->

        <!-- Content -->
        <div class="article__content">
            <div class="container">
                <div class="row">
                    <div class="boxes boxes--start">

                        <?php $catquery = new WP_Query( 'cat=28&posts_per_page=-1' ); ?>

                        <?php while($catquery->have_posts()) : $catquery->the_post(); ?>

                            <div class="box box--3">
                                <a class="box__link" href="<?php echo get_permalink();?>"></a>
                                <div class="box__header">
                                    <?php the_post_thumbnail();?>
                                </div>
                                <div class="box__body">
                                    <h3><?php echo get_the_title();?></h3>
                                </div>
                            </div>
                            
                        <?php endwhile; ?>

                    </div>
                </div>
            </div>
        </div>
        <!-- / -->

    </article>
    <!-- / -->

<?php get_footer(); ?>