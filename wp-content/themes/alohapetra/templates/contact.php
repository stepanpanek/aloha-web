﻿<?php /* Template Name: Contact */ ?>

<?php get_header(); ?>

    <!-- Article -->
    <article class="article article--contact">

        <!-- Header -->
        <header class="article__header lazy" data-bg="<?php echo get_the_post_thumbnail_url();?>">
            
            <!-- Hello -->
            <section id="hello" class="section section--hello">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <h1><?php the_field('hello__title');?></h1>
                            <a class="scrollDown" href="#content">
                                <img class="lazy" data-src="<?php echo get_template_directory_uri();?>/img/arrow-down.svg" alt="Šipka dolů">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="section--hello__bg">
                    <img class="lazy" data-src="<?php the_field('hello__img');?>" alt="Aloha Petra úvodní obrázek">
                </div>
            </section>
            <!-- / -->

        </header>
        <!-- / -->

        <!-- Content -->
        <div class="article__content">
            <div class="container">
                <p class="introText">Kontaktní údaje</p>
                <div class="row article--contact__intro">
                    <div class="col-md-4">
                        <h2>Napište mi nebo zavolejte.</h2>
                        <p>Ráda vám zodpovím všechny vaše dotazy. Vzdálenost nehraje roli. Naše spolupráce může být osobní i na dálku.</p>
                        <ul>
                            <li><a href="tel:+420732266999">
                                <img class="lazy" data-src="<?php echo get_template_directory_uri();?>/img/icon-phone.svg" alt="Phone">
                                +420 732 266 999
                            </a></li>
                            <li><a href="mailto:info@alohapetra.cz">
                                <img class="lazy" data-src="<?php echo get_template_directory_uri();?>/img/icon-email.svg" alt="Email">
                                info@alohapetra.cz
                            </a></li>
                        </ul>
                    </div>
                    <div class="col-md-8">
                        <?php echo do_shortcode('[contact-form-7 id="430" title="Kontakt"]');?>
                    </div>
                </div>
                <hr>
                <p class="introText">Přijďte do mé poradny</p>
                <div class="row article--contact__address">
                    <div class="col-md-9">
                        <img class="lazy" data-src="<?php echo get_template_directory_uri();?>/img/address.jpg" alt="Poradna">
                        <div class="box">
                            <h3>Nádražní 47/90, <br />
                                150 00 Praha 5-Smíchov
                            </h3>
                            <p>Poradna je součástí Institutu zdraví Duatela s.r.o.</p>
                            <p>Informace o metru Anděl a skvělé dostupnosti</p>
                            <a href="https://www.google.cz/maps/place/N%C3%A1dra%C5%BEn%C3%AD+47,+150+00+Praha+5-Sm%C3%ADchov/@50.0691463,14.4040126,17z/data=!3m1!4b1!4m5!3m4!1s0x470b9459d7c18759:0x11a5ff8665699492!8m2!3d50.0691463!4d14.4062013" target="_blank"><span>Navigovat</span> <img class="lazy" data-src="<?php echo get_template_directory_uri();?>/img/arrow-right.svg" alt="Šipka doleva"></a>
                        </div>
                    </div>
                </div>
                <div class="row article--contact__info">
                    <div class="col-md-5">
                        <h4>Údaje</h4>
                        <ul>
                            <li>Mgr. Petra Wurmová</li>
                            <li>Zádušní 2925</li>
                            <li>Mělník 276 01</li>
                        </ul>
                    </div>
                    <div class="col-md-5">
                        <h4>IČO & BANKOVNÍ ÚČET</h4>
                        <ul>
                            <li>IČ: 08495238. Nejsem plátce DPH</li>
                            <li>Bú: 294843969/0300, ČSOB</li>
                        </ul>
                    </div>
                    <div class="col-md-2 article--contact__socials">
                        <ul>
                            <li><a href="https://www.facebook.com/alohapetra.cz" target="_blank"><img class="lazy" data-src="<?php echo get_template_directory_uri();?>/img/icon-fb.svg" alt="Ikona FB"></a></li>
                            <li><a href="https://www.instagram.com/alohapetra.cz/" target="_blank"><img class="lazy" data-src="<?php echo get_template_directory_uri();?>/img/icon-ig.svg" alt="Ikona IG"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- / -->

    </article>
    <!-- / -->

<?php get_footer(); ?>