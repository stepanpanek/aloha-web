﻿<?php /* Template Name: Esences */ ?>

<?php get_header(); ?>

    <!-- Article -->
    <article class="article article--service article--service--essences">

        <!-- Header -->
        <header class="article__header lazy" data-bg="<?php echo get_the_post_thumbnail_url();?>">
            
            <!-- Hello -->
            <section id="hello" class="section section--hello">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <h1><?php the_field('hello__title');?></h1>
                            <a class="scrollDown" href="#content">
                                <img class="lazy" data-src="<?php echo get_template_directory_uri();?>/img/arrow-down.svg" alt="Šipka dolů">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="section--hello__bg">
                    <img class="lazy" data-src="<?php the_field('hello__img');?>" alt="Aloha Petra úvodní obrázek">
                </div>
            </section>
            <!-- / -->

        </header>
        <!-- / -->
        
        <!-- Content -->
        <div class="article__content">

            <div class="article--service__intro">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="introText">Objevte</p>
                            <h2><?php the_field('service__title');?></h2>
                            <p><?php the_field('service__desc');?></p>
                            <div class="boxes boxes--between">
                                <div class="box box--3">
                                    <span>01</span>
                                    <p><?php the_field('service__ad--1');?></p>
                                </div>
                                <div class="box box--3">
                                    <span>02</span>
                                    <p><?php the_field('service__ad--2');?></p>
                                </div>
                                <div class="box box--3">
                                    <span>03</span>
                                    <p><?php the_field('service__ad--3');?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="article--service__way">
                <div class="container">
                    <h2 class="introText">Jak vypadá naše společná cesta</h2>
                    <div class="row">

                        <?php if( have_rows('service--way__rep') ): ?>

                            <div class="col-md-12 boxes">

                            <?php while( have_rows('service--way__rep') ): the_row(); 

                                // vars
                                $serviceWayImg = get_sub_field('service--way__rep__img');
                                $serviceWayName = get_sub_field('service--way__rep__name');
                                $serviceWayDesc = get_sub_field('service--way__rep__desc');

                                ?>

                                <div class="box box--1"> 
                                    <div class="box__header">
                                        <img class="lazy" data-src="<?php echo $serviceWayImg; ?>" alt="Výhoda">
                                    </div>
                                    <div class="box__body">
                                        <h3><?php echo $serviceWayName; ?></h3>
                                        <p><?php echo $serviceWayDesc; ?></p>
                                    </div> 
                                </div>

                            <?php endwhile; ?>

                            </div>

                        <?php endif; ?>

                    </div>
                </div>
            </div>

            <!-- Quotation -->
            <div class="article--service__quotation section--about">
                <div class="container">
                    <div class="row">
                        <div class="offset-md-5 col-md-7">
                            <div class="section--about__content">
                                <h2><?php the_field('quotation__text'); ?></h2>
                                <p><?php the_field('quotation__author'); ?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section--about__bg">
                    <img class="lazy" data-src="<?php the_field('quotation__img');?>" alt="Mgr. Petra Wurmová">
                </div>
            </div>
            <!-- / -->

            <!-- Additional text -->
            <div class="article--service__program">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4">
                            <h2 class="introText">Pro koho jsou esence vhodné</h2>
                        </div>
                        <div class="col-lg-8">
                            <div class="contentWrapper">
                                <?php the_field('program__text');?>

                                <div class="btn-wrapper">
                                    <a href="<?php echo get_site_url(); ?>/cenik" class="btn btn--primary"><span>Prohlédnout ceník</span></a>
                                    <a href="<?php echo get_site_url(); ?>/udalosti" class="btn btn--secondary"><span>Objednat konzultaci</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- / -->

            <!-- Program -->
            <div class="article--service__program">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4">
                            <h2 class="introText">S čím Vám esence pomůžou</h2>
                        </div>
                        <div class="col-lg-8">
                            <div class="contentWrapper">
                                
                                <?php if( have_rows('service__program__rep') ): ?>

                                    <ul>

                                    <?php while( have_rows('service__program__rep') ): the_row(); 

                                        // vars
                                        $progrCheck = get_sub_field('service__program__check');

                                        ?>

                                        <li><?php echo $progrCheck;?></li>

                                    <?php endwhile; ?>

                                    </ul>

                                <?php endif; ?>

                                <div class="btn-wrapper">
                                    <a href="<?php echo get_site_url(); ?>/cenik" class="btn btn--primary"><span>Prohlédnout ceník</span></a>
                                    <a href="<?php echo get_site_url(); ?>/udalosti" class="btn btn--secondary"><span>Objednat konzultaci</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- / -->

        </div>
        <!-- / -->

		<!-- Fixed CTA -->
		<div class="fixedCTA fixedCTA--fb">
            <a href="https://m.me/alohapetra.cz" target="_blank">
                <span>Napsat na Messenger</span>
                <img class="lazy" data-src="<?php echo get_template_directory_uri();?>/img/icon-fb-rounded.svg" alt="Ikona pro messenger">
            </a>	
		</div>
		<!-- / -->

		<!-- Fixed CTA -->
		<div class="fixedCTA">
            <a href="tel:+420732266999">
                <span>+420 732 266 999</span>
                <img class="lazy" data-src="<?php echo get_template_directory_uri();?>/img/icon-phone-rounded.svg" alt="Ikona pro telefon">
            </a>	
		</div>
		<!-- / -->

    </article>
    <!-- / -->

<?php get_footer(); ?>