﻿<?php /* Template Name: Frontpage */ ?>

<?php get_header(); ?>

    <!-- Hello -->
    <section id="hello" class="section section--hello">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <h1><?php the_field('hello__title');?></h1>
                    <p><?php the_field('hello__desc');?></p>
                    <a class="btn btn--primary" href="#"><span>Konzultace zdarma</span></a>
                </div>
            </div>
        </div>
        <div class="section--hello__bg">
            <img class="lazy" data-src="<?php the_field('hello__img');?>" alt="Aloha Petra úvodní obrázek">
        </div>
    </section>
    <!-- / -->

    <!-- Intro -->
    <section id="intro" class="section section--intro">
        <div class="container">      
            <div class="row">
                <div class="col-md-12">
                    <p class="introText">S čím vám pomůžu</p>
                    <h2><?php the_field('intro__title');?></h2>
                </div>
                <div class="col-md-6">
                    <p><?php the_field('intro__desc--left');?></p>
                </div>
                <div class="col-md-6">
                    <p><?php the_field('intro__desc--right');?></p>
                </div>
            </div>
            <div class="row section--intro__benefits">

                <?php if( have_rows('benefits__rep') ): ?>

                    <div class="col-md-12 boxes">

                    <?php while( have_rows('benefits__rep') ): the_row(); 

                        // vars
                        $benefitImg = get_sub_field('benefits__img');
                        $benefitName = get_sub_field('benefits__name');

                        ?>

                        <div class="box">     
                            <div class="img-wrapper">
                                <img class="lazy" data-src="<?php echo $benefitImg; ?>" alt="Výhoda">
                            </div>
                            <h3><?php echo $benefitName; ?></h3>
                        </div>

                    <?php endwhile; ?>

                    </div>

                <?php endif; ?>

            </div>
        </div>
    </section>
    <!-- / -->

    <!-- Services -->
    <section id="services" class="section section--services">
        <div class="container">
            <h2 class="introText">Přehled služeb</h2>
            <div class="row">
            <?php if( have_rows('service__rep') ): ?>

                <div class="col-md-12 boxes boxes--start">

                <?php while( have_rows('service__rep') ): the_row(); 

                    // vars
                    $servImg = get_sub_field('service__img');
                    $servName = get_sub_field('service__name');
                    $servSpan = get_sub_field('service__span');
                    $servDesc = get_sub_field('service__desc');
                    $servLink = get_sub_field('service__link');

                    ?>

                    <div class="box box--1">
                        <div class="box__header">
                            <?php
                                $image = get_sub_field('service__img', false);
                                $size = 'gallery';

                                echo wp_get_attachment_image( $image, $size, false, array(
                                    'src' => '',
                                    'srcset' => '',
                                    'class' => 'lazy',
                                    'data-src' => wp_get_attachment_image_url( $image, $size ),
                                ) );
                            ?>
                        </div>
                        <div class="box__body">
                            <h3><?php echo $servName; ?></h3>
                            <span><?php echo $servSpan; ?></span>
                            <p><?php echo $servDesc; ?></p>
                            <div class="btn-wrapper">
                                <a class="btn btn--primary" href="<?php echo get_home_url();?>/sluzby<?php echo $servLink; ?>"><span>Zjistit více</span></a>
                            </div>
                        </div>
                    </div>

                <?php endwhile; ?>

                </div>

                <?php endif; ?>

            </div>
        </div>
    </section>
    <!-- / -->

    <!-- For you -->
    <section id="foryou" class="section section--forYou">
        <div class="container">
            <h2 class="introText">Pořádám pro vás</h2>
            <div class="row">
       
            <?php if( have_rows('forYou__rep') ): ?>

                <div class="col-md-12 boxes boxes--between">

                <?php while( have_rows('forYou__rep') ): the_row(); 

                    // vars
                    $forYouImg = get_sub_field('forYou__img');
                    $forYouName = get_sub_field('forYou__name');

                    ?>

                    <div class="box box--4">
                        <div class="img-wrapper">
                            <img class="lazy" data-src="<?php echo $forYouImg; ?>" alt="<?php echo $forYouName; ?>">
                        </div>
                        <h3><?php echo $forYouName; ?></h3>
                    </div>

                <?php endwhile; ?>

                </div>

            <?php endif; ?>

            </div>

            <div class="btn-wrapper">
                <a href="<?php echo get_site_url(); ?>/udalosti" class="btn btn--primary"><span>Zobrazit události</span></a>
                <a href="<?php echo get_site_url(); ?>/kontakt/#formular" class="btn btn--secondary">Mám zájem o akci</a>
            </div>
        </div>
    </section>
    <!-- / -->

    <!-- Recipes introduction -->
    <section class="section section--recipes">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <small class="introText">Objevte online sbírku receptů</small>
                    <h2><span class="colored">200 +</span> <span>receptů, článků a praktických materiálů</span></h2>
                    <p><?php the_field('recipes__desc');?></p>
                    <div class="btn-wrapper">
                        <a class="btn btn--secondary" href="<?php echo get_site_url(); ?>/recepty">Premium členství</a>
                        <a class="btn btn--primary" href="<?php echo get_site_url(); ?>/recepty"><span>Ukázka receptů</span></a>
                    </div>
                </div>
                <div class="col-md-6 section--recipes__img">
                    <img class="lazy" data-src="<?php echo get_template_directory_uri(); ?>/img/recipes.png" alt="Přehled receptů">
                </div>
            </div>
        </div>
    </section>
    <!-- / -->

    <!-- Testimonials -->
    <section class="section section--testimonials">
        <div class="container container--large">
            <h2 class="introText">Recenze a příběhy mých klientů</h2>
            <!-- Swiper -->
            <div class="swiper-container-hp">
                <div class="swiper-wrapper">

                    <?php $catquery = new WP_Query( 'post_type=pribehy&posts_per_page=3' ); ?>

                    <?php while($catquery->have_posts()) : $catquery->the_post(); ?>
                           
                    <div class="swiper-slide">
                        <div class="box">
                            <div class="box__body">

                                <div class="img-wrapper">              
                                    <?php
                                        $image = the_post_thumbnail();
                                        $size = 'gallery';

                                        echo wp_get_attachment_image( $image, $size, false, array(
                                            'src' => false,
                                            'srcset' => false,
                                            'class' => 'lazy',
                                            'data-src' => wp_get_attachment_image_url( $image, $size ),
                                        ) );
                                    ?>
                                </div>

                                <h3><?php the_title();?></h3>
                                <span><?php the_field('testimonials__position') ?></span>

                            </div>
                            <div class="box__header">
                                <div class="box__content">
                                    <p><?php the_excerpt(5); ?></p>
                                </div>
                                <a href="<?php echo get_post_permalink();?>">Pokračovat ve čtení</a>
                            </div>
                        </div>
                    </div>
                        
                    <?php wp_reset_postdata(); ?>
                    <?php endwhile; ?>

                </div>
                <nav class="swiper-nav">
                    <div class="prev">
                        <img class="lazy" data-src="<?php echo get_template_directory_uri();?>/img/arrow-left.svg">
                    </div>
                    <div class="next">
                        <img class="lazy" data-src="<?php echo get_template_directory_uri();?>/img/arrow-right.svg">
                    </div>
                </nav>
            </div>
            <!-- / -->
            <div class="btn-wrapper">
                <a href="<?php echo get_site_url(); ?>/pribehy" class="btn btn--primary"><span>Všechny příběhy</span></a>
            </div>
        </div>
    </section>
    <!-- / -->

    <!-- About -->
    <section class="section section--about">
        <div class="container">
            <div class="row">
                <div class="offset-md-5 col-md-7">
                    <div class="section--about__content">
                        <h2><?php the_field('about__name'); ?></h2>
                        <?php the_field('about__desc'); ?>
                        <div class="btn-wrapper">
                            <a class="btn btn--primary" href="<?php echo get_site_url(); ?>/o-mne"><span>Zjistit více</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section--about__bg">
            <img class="lazy" data-src="<?php the_field('about__img');?>" alt="Mgr. Petra Wurmová">
        </div>
    </section>
    <!-- / -->

<?php get_footer(); ?>