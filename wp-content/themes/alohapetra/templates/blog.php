﻿<?php /* Template Name: Blog */ ?>

<?php get_header(); ?>

	<!-- Article -->	
	<article class="article article--recipes">

		<!-- Header -->
		<header class="article__header lazy" data-bg="<?php echo get_the_post_thumbnail_url();?>">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<p class="introText">Objevte</p>
						<h1>Mé články</h1>
					</div>
				</div>
			</div>
		</header>
		<!-- / -->
		
		<!-- Content -->
		<div id="content" class="article__content">
			<div class="container">
				<div class="row">
					<div class="col-md-12 article--recipes__sorting">
						<div id="orderBy">
							<p>Seřadit podle: 
								<span>Nejnovější</span>
								<img class="lazy" data-src="<?php echo get_template_directory_uri();?>/img/icon-more.svg" alt="Šipka dolů">							
							</p>
							<ul>
								<li class="recipes__sorting" attr="1">Nejnovější</li>
								<li class="recipes__sorting" attr="2">Nejstarší</li>
								<li class="recipes__sorting" attr="3">Název A-Z</li>
								<li class="recipes__sorting" attr="4">Název Z-A</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="row">

					<div class="col-lg-4">
					
						<!-- Sidebar -->
                       <form action="<?php echo site_url() ?>/wp-admin/admin-ajax.php" method="POST" id="filter">
                       <input type="hidden" name="type" value="blog">
						<aside id="sidebar" class="aside aside--sidebar" role="navigation">
							<div class="block block--search">
								<label for="search">
									<input type="search" placeholder="Hledat článek" name="search" id="search">
									<a href="#" class="btn" id="searchRecipe">Vyhledat</a>
								</label>
							</div>
							<div class="block block--category">
								<div class="block__header">
									<h2>Kategorie</h2>
								</div>
								<div class="block__body">
									<?php
									$categories = get_terms([
										'taxonomy' => 'category',
										'hide_empty' => false,
										'parent'  => 28 
									]);
									
									foreach($categories as $category){
									?>
								  	<label for="category[]">
									  	<span><?= $category->name ?></span>
										<input type="checkbox" name="category[]" value="<?= $category->term_id ?>" class="category__filter">
										<div></div>
									</label>
                                  <?php } ?>

								</div>
							</div>
							<div class="block block--category">
								<div class="block__header">
									<h2>Vyhledávání <br /> podle štítků</h2>
								</div>
								<div class="block__body">
									<?php $materials = get_terms([
										'taxonomy' => 'post_tag',
										'hide_empty' => false,
										/*'parent'  => 8 */
										]);
										
									foreach($materials as $material){
									?>
									<label for="category[]">
										<span><?= $material->name ?></span>
										<input type="checkbox" name="materials[]" value="<?= $material->term_id ?>" class="category__filter">
										<div></div>
									</label>
                                  <?php } ?>

								</div>
							</div>
							<div class="block block--reset">
								<div id="resetFilter">
									<img class="lazy" data-src="<?php echo get_template_directory_uri(); ?>/img/icon-close.svg" alt="Zrušit filtr">
									<span>Zrušit filtr</span>
								</div>
							</div>
						</aside>
                        <input type="hidden" name="action" value="myfilter">
                        <input type="hidden" name="paged" id="paged">
                        <input type="hidden" name="sorted" id="sorted">
                        </form>
						<!-- / -->

					</div>

					<div class="col-lg-8">

						<div class="boxes boxes--start">

							<?php $catquery = new WP_Query( 'post_type=post&posts_per_page=10' ); ?>
							<?php while($catquery->have_posts()) : $catquery->the_post(); ?>
	                        <?php $is_premium = pms_is_post_restricted( get_the_ID() ) ? 'is-premium'  : FALSE;?>
								<div class="box box--2 <?= $is_premium ?>">
									<a class="box__link" href="<?php echo get_permalink();?>"></a>
									<div class="box__header">
										<img class="lazy" data-src="<?php the_post_thumbnail_url();?>" alt="<?php echo get_permalink();?>">
									</div>
									<div class="box__body">
										<h3><?php echo get_the_title();?></h3>
									</div>
									<div class="box__premium">
										<img src="<?php echo get_template_directory_uri(); ?>/img/icon-locked.svg" alt="Premium recept">
										<h4>Premium recept</h4>
										<a href="<?php the_permalink();?>">Zakoupit členství</a>
									</div>
								</div>
								<?php wp_reset_postdata(); ?>
							<?php endwhile; ?>

                       	 	<div class="col-md-12 box__pagination">
								<?php
                                    $page = isset($_POST['paged']) ? $_POST['paged'] : 1;
									/*$page = $_POST['paged'];
									$paged = ( $page ) ? $page : 1;  */
									echo paginate_links( array(
										'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
										'total'        => $catquery->max_num_pages,
										'current'      => max( 1, $page ),
										'format'       => '?paged=%#%',
										'show_all'     => false,
										'type'         => 'plain',
								
									) );
								?>
							</div>
						</div>

					</div>

				</div>
			</div>
		</div>
		<!-- / -->

	</article>
	<!-- / -->

<?php get_footer(); ?>