﻿<?php get_header() ?>

	<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : the_post() ?>

	<!-- Article -->	
	<article class="article article--singlePost">

		<!-- Header -->
		<header class="article__header">
			
			<div class="article__header__intro">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<a href="<?php echo get_site_url();?>/blog" id="back">
								<img class="lazy" data-src="<?php echo get_template_directory_uri();?>/img/arrow-left.svg" alt="Šipka zpět">
								<span>Zpět na blog</span>
							</a>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<p class="introText">Článek</p>
							<h1><?php the_title(); ?></h1>
						</div>
					</div>
				</div>
			</div>

			<div class="article__header__img">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<img src="<?php echo get_the_post_thumbnail_url();?>" alt="Fotka">
						</div>
					</div>
				</div>
			</div>

		</header>
		<!-- / -->
		
		<!-- Content -->
		<div class="article__content">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		</div>
		<!-- / -->

	</article>
	<!-- / -->

		<?php endwhile ?>
	<?php endif ?>

<?php get_footer() ?>