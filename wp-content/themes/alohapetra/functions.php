<?php

define( 'THEME_DIRECTORY', get_template_directory() );

/**
 * Theme Support and Site Settings
 */
require_once THEME_DIRECTORY . '/inc/site-settings.php';

/**
 * Register Sidebars
 */
require_once THEME_DIRECTORY . '/inc/sidebars-widgets.php';

/**
 * Shortcodes
 */
require_once THEME_DIRECTORY . '/inc/shortcodes.php';

/**
 * Scripts and Styles
 */
require_once THEME_DIRECTORY . '/inc/javascript.php';

/**
 * Cleanup WordPress and Reorder menus
 */
require_once THEME_DIRECTORY . '/inc/cleanup-reorder.php';

/**
 * Customizer
 */
require_once THEME_DIRECTORY . '/inc/customizer.php';

/*
 * Ecerpt symbol
 */ 
function new_excerpt_more( $more ) {
    return ' ..';
}
add_filter('excerpt_more', 'new_excerpt_more');

// Excerpt lenght
function excerpt($limit) {
	$excerpt = explode(' ', get_the_excerpt(), $limit);
	if (count($excerpt)>=$limit) {
	  array_pop($excerpt);
	  $excerpt = implode(" ",$excerpt).'...';
	} else {
	  $excerpt = implode(" ",$excerpt);
	} 
	$excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
	return $excerpt;
  }

  function content($limit) {
	$content = explode(' ', get_the_content(), $limit);
	if (count($content)>=$limit) {
	  array_pop($content);
	  $content = implode(" ",$content).'...';
	} else {
	  $content = implode(" ",$content);
	} 
	$content = preg_replace('/\[.+\]/','', $content);
	$content = apply_filters('the_content', $content); 
	$content = str_replace(']]>', ']]&gt;', $content);
	return $content;
  }

/**
 * Theme Options Page
 */
require_once THEME_DIRECTORY . '/inc/theme-options-page.php';

if ( ! function_exists( 'log_me' ) ) :
	/**
	 * Simple error logging
	 *
	 * @param $message
	 * @return bool
	 */
	function log_me( $message )
	{
		if ( true !== WP_DEBUG ) return false;

		if ( is_array($message) || is_object($message) ) {
			return error_log( json_encode($message) );
		}

		return error_log( $message );
	}

endif;


if ( ! function_exists( 'extend_array' ) ) :

	/**
	 * jQuery style array extend
	 *
	 * @return array
	 */
	function extend_array()
	{
		$args     = func_get_args();
		$extended = array();

		if ( is_array( $args ) && count( $args ) )
		{
			foreach ( $args as $array )
			{
				if ( ! is_array( $array ) )	continue;
				$extended = array_merge( $extended, $array );
			}
		}

		return $extended;
	}

endif;

// Add home class to homepage
function home_body_class($classes) {
    if ( is_front_page() ) {
        $classes[] = 'home';
    }

    return $classes;
}

add_filter( 'body_class', 'home_body_class' );

// Custom image size
add_action( 'after_setup_theme', 'wpdocs_theme_setup' );
function wpdocs_theme_setup() {
    add_image_size( 'gallery', 600 ); // 300 pixels wide (and unlimited height)
    add_image_size( 'homepage-thumb', 220, 180, true ); // (cropped)
}

//Remove Gutenberg Block Library CSS from loading on the frontend
function smartwp_remove_wp_block_library_css(){
	wp_dequeue_style( 'wp-block-library' );
	wp_dequeue_style( 'wp-block-library-theme' );
	wp_dequeue_style( 'wc-block-style' ); // Remove WooCommerce block CSS
}
add_action( 'wp_enqueue_scripts', 'smartwp_remove_wp_block_library_css', 100 );

// Woocommerce support
add_action( 'after_setup_theme', function() {
	add_theme_support( 'woocommerce' );
} );

// Remove all Woo Styles
add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

// Custom excerpt
class Excerpt {

	// Default length (by WordPress)
	public static $length = 55;
  
	// So you can call: my_excerpt('short');
	public static $types = array(
		'short' => 25,
		'regular' => 55,
		'long' => 100
	  );
  
	/**
	 * Sets the length for the excerpt,
	 * then it adds the WP filter
	 * And automatically calls the_excerpt();
	 *
	 * @param string $new_length 
	 * @return void
	 * @author Baylor Rae'
	 */
	public static function length($new_length = 55) {
	  Excerpt::$length = $new_length;
  
	  add_filter('excerpt_length', 'Excerpt::new_length');
  
	  Excerpt::output();
	}
  
	// Tells WP the new length
	public static function new_length() {
	  if( isset(Excerpt::$types[Excerpt::$length]) )
		return Excerpt::$types[Excerpt::$length];
	  else
		return Excerpt::$length;
	}
  
	// Echoes out the excerpt
	public static function output() {
	  the_excerpt();
	}
  
  }
  
  // An alias to the class
  function my_excerpt($length = 55) {
	Excerpt::length($length);
  }

// New taxonomy – recipes
function wpdocs_kantbtrue_init() {
$labels = array(
	'name'                  => _x( 'Recepty', 'Post type general name', 'recepty' ),
	'singular_name'         => _x( 'Recept', 'Post type singular name', 'recepty' ),
	'menu_name'             => _x( 'Recepty', 'Admin Menu text', 'recepty' ),
	'name_admin_bar'        => _x( 'Recept', 'Add New on Toolbar', 'recepty' ),
	'add_new'               => __( 'Nový recept', 'recepty' ),
	'add_new_item'          => __( 'Přidat nový recept', 'recepty' ),
	'new_item'              => __( 'Nový recept', 'recepty' ),
	'edit_item'             => __( 'Editovat recept', 'recepty' ),
	'view_item'             => __( 'Zobrazit recept', 'recepty' ),
	'all_items'             => __( 'Všechny recepty', 'recepty' ),
	'search_items'          => __( 'Vyhledat recepty', 'recepty' ),
	'parent_item_colon'     => __( 'Parent recipes:', 'recepty' ),
	'not_found'             => __( 'Recepty nenalezeny.', 'recepty' ),
	'not_found_in_trash'    => __( 'V koši se nenachází žádné recepty.', 'recepty' ),
	'featured_image'        => _x( 'Zvolit náhledový obrázek', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'recepty' ),
	'set_featured_image'    => _x( 'Zvolit náhledový obrázek', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'recepty' ),
	'remove_featured_image' => _x( 'Smazat obrázek', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'recepty' ),
	'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'recepty' ),
	'archives'              => _x( 'Recipe archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'recepty' ),
	'insert_into_item'      => _x( 'Insert into recipe', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'recepty' ),
	'uploaded_to_this_item' => _x( 'Uploaded to this recipe', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'recepty' ),
	'filter_items_list'     => _x( 'Filter recipes list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'recepty' ),
	'items_list_navigation' => _x( 'Recipes list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'recepty' ),
	'items_list'            => _x( 'Recipes list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'recepty' ),
);     
$args = array(
	'labels'             => $labels,
	'description'        => 'Recepty od Alohy Petry',
	'public'             => true,
	'publicly_queryable' => true,
	'menu_icon'           => 'dashicons-carrot',
	'show_ui'            => true,
	'show_in_menu'       => true,
	'query_var'          => true,
	'rewrite'            => array( 'slug' => 'recept' ),
	'capability_type'    => 'post',
	'has_archive'        => true,
	'hierarchical'       => true,
	'menu_position'      => 0,
	'supports'           => array( 'title', 'editor', 'category', 'tags', 'author', 'thumbnail' ),
	'taxonomies'         => array( 'kategorie', 'post_tag' ),
	'show_in_rest'       => true,
	'taxonomies'         => array( 'category' ),
);

	
register_post_type( 'recepty', $args );
}

add_action( 'init', 'wpdocs_kantbtrue_init' );

add_action( 'init', 'recepty_taxonomies', 0 );

function recepty_taxonomies() {
    register_taxonomy( 'recepty-tags', 'recepty', array( 'hierarchical' => false, 'label' => 'Štítky', 'query_var' => true, 'rewrite' => true,'show_ui' => true, ) );
}


// New taxonomy Stories
function wpdocs_kantbtrue_stories() {

$labels = array(
	'name'                  => _x( 'Příběhy', 'Post type general name', 'pribehy' ),
	'singular_name'         => _x( 'Příběh', 'Post type singular name', 'pribehy' ),
	'menu_name'             => _x( 'Příběhy', 'Admin Menu text', 'pribehy' ),
	'name_admin_bar'        => _x( 'Příběh', 'Add New on Toolbar', 'pribehy' ),
	'add_new'               => __( 'Nový příběh', 'pribehy' ),
	'add_new_item'          => __( 'Přidat nový příběh', 'pribehy' ),
	'new_item'              => __( 'Nový příběh', 'pribehy' ),
	'edit_item'             => __( 'Editovat příběh', 'pribehy' ),
	'view_item'             => __( 'Zobrazit příběh', 'pribehy' ),
	'all_items'             => __( 'Všechny příběhy', 'pribehy' ),
	'search_items'          => __( 'Vyhledat příběhy', 'pribehy' ),
	'parent_item_colon'     => __( 'Parent recipes:', 'pribehy' ),
	'not_found'             => __( 'Příběhy nenalezeny.', 'pribehy' ),
	'not_found_in_trash'    => __( 'V koši se nenachází žádné příběhy.', 'pribehy' ),
	'featured_image'        => _x( 'Zvolit náhledový obrázek', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'pribehy' ),
	'set_featured_image'    => _x( 'Zvolit náhledový obrázek', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'pribehy' ),
	'remove_featured_image' => _x( 'Smazat obrázek', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'pribehy' ),
	'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'pribehy' ),
	'archives'              => _x( 'Recipe archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'pribehy' ),
	'insert_into_item'      => _x( 'Insert into recipe', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'pribehy' ),
	'uploaded_to_this_item' => _x( 'Uploaded to this recipe', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'pribehy' ),
	'filter_items_list'     => _x( 'Filter recipes list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'pribehy' ),
	'items_list_navigation' => _x( 'Recipes list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'pribehy' ),
	'items_list'            => _x( 'Recipes list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'pribehy' ),
);     
$args = array(
	'labels'             => $labels,
	'description'        => 'Příběhy klientů',
	'public'             => true,
	'publicly_queryable' => true,
	'menu_icon'           => 'dashicons-format-image',
	'show_ui'            => true,
	'show_in_menu'       => true,
	'query_var'          => true,
	'rewrite'            => array( 'slug' => 'pribeh' ),
	'capability_type'    => 'post',
	'has_archive'        => true,
	'hierarchical'       => true,
	'menu_position'      => 0,
	'supports'           => array( 'title', 'editor', 'category', 'author', 'thumbnail' ),
	'taxonomies'         => array( 'kategorie', 'post_tag' ),
	'show_in_rest'       => true,
	'taxonomies'          => array( 'category' ),
);
	
register_post_type( 'pribehy', $args );
}
add_action( 'init', 'wpdocs_kantbtrue_stories' );

 
add_action('wp_ajax_myfilter', 'misha_filter_function'); // wp_ajax_{ACTION HERE} 
add_action('wp_ajax_nopriv_myfilter', 'misha_filter_function');
 
function misha_filter_function(){
	$args = array(
		'orderby' => 'date', // we will sort posts by date
		'order'	=> $_POST['date'] // ASC or DESC
	);
        if($_POST['type'] == 'recepies'){
            echo get_template_part('templates/recepty','sablona-ajax'); 
        }
        elseif($_POST['type'] == 'blog'){
            echo get_template_part('templates/blog','ajax');         
        }
    
	die();
}