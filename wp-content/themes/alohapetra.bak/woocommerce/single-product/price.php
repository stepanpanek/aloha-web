<?php
/**
 * Single Product Price
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

?>

<!-- Price template -->
<div class="row">
	<div class="col-md-12">
		<h2>Adresa a místo konání</h2>
		<p>
			<?php the_field('product__street');?>, <?php the_field('product__city');?><?php if( get_field('product__add') ): ?>, <?php the_field('product__add');?><?php endif; ?>
		</p>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<h2>Datum a doba trvání semináře</h2>
		<p><?php the_field('product__date');?>, <?php the_field('product__last');?></p>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<h2>Kapacita osob semináře</h2>
		<p><?php the_field('product__people');?> osob</p>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<h2>Cena semináře s DPH</h2>
		<p class="<?php echo esc_attr( apply_filters( 'woocommerce_product_price_class', 'price' ) ); ?>"><?php echo $product->get_price_html(); ?></p>
	</div>
</div>
<!-- / Price t. -->