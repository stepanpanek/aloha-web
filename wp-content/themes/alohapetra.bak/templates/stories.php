﻿<?php /* Template Name: Stories */ ?>

<?php get_header(); ?>

    <!-- Article -->
    <article class="article article--stories">

        <!-- Header -->
        <header class="article__header lazy" data-bg="<?php echo get_the_post_thumbnail_url();?>">
            
            <!-- Hello -->
            <section id="hello" class="section section--hello">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <h1><?php the_field('hello__title');?></h1>
                            <a class="scrollDown" href="#content">
                                <img class="lazy" data-src="<?php echo get_template_directory_uri();?>/img/arrow-down.svg" alt="Šipka dolů">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="section--hello__bg">
                    <img class="lazy" data-src="<?php the_field('hello__img');?>" alt="Aloha Petra úvodní obrázek">
                </div>
            </section>
            <!-- / -->

        </header>
        <!-- / -->

        <!-- Content -->
        <div class="article__content">

            <!-- Testimonials -->
            <section class="section section--testimonials">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">

                            <?php $catquery = new WP_Query( 'post_type=pribehy&posts_per_page=3' ); ?>

                            <?php while($catquery->have_posts()) : $catquery->the_post(); ?>
                                
                                <div class="box">
                                    <div class="box__body">

                                        <div class="img-wrapper">              
                                            <?php
                                                $image = the_post_thumbnail();
                                                $size = 'gallery';

                                                echo wp_get_attachment_image( $image, $size, false, array(
                                                    'src' => false,
                                                    'srcset' => false,
                                                    'class' => 'lazy',
                                                    'data-src' => wp_get_attachment_image_url( $image, $size ),
                                                ) );
                                            ?>
                                        </div>

                                        <h3><?php the_title();?></h3>
                                        <span><?php the_field('testimonials__position') ?></span>

                                    </div>
                                    <div class="box__header">
                                        <div class="box__content">
                                            <p><?php the_excerpt(5); ?></p>
                                        </div>
                                        <a href="<?php the_permalink();?>">Pokračovat ve čtení</a>
                                    </div>
                                </div>
                                
                            <?php wp_reset_postdata(); ?>
                            <?php endwhile; ?>
                            
                    </div>
                </div>
            </section>
            <!-- / -->

        </div>
        <!-- / -->

    </article>
    <!-- / -->

<?php get_footer(); ?>