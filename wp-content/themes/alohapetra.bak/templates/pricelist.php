﻿<?php /* Template Name: PriceList */ ?>

<?php get_header(); ?>

	<!-- Article -->	
	<article class="article article--priceList">

		<!-- Header -->
		<header class="article__header lazy" data-bg="<?php echo get_the_post_thumbnail_url();?>">
			
		    <!-- Hello -->
			<section id="hello" class="section section--hello">
				<div class="container">
					<div class="row">
						<div class="col-md-8">
							<h1><?php the_field('hello__title');?></h1>
							<a class="scrollDown" href="#content">
								<img class="lazy" data-src="<?php echo get_template_directory_uri();?>/img/arrow-down.svg" alt="Šipka dolů">
							</a>
						</div>
					</div>
				</div>
				<div class="section--hello__bg">
					<img class="lazy" data-src="<?php the_field('hello__img');?>" alt="Aloha Petra úvodní obrázek">
				</div>
			</section>
			<!-- / -->

		</header>
		<!-- / -->
		
		<!-- Content -->
		<div id="content" class="article__content">
			<div class="container">

				<section class="article--priceList__single">
					<p class="introText">Objevte</p>
					<h2>Ceník služeb</h2>

					<?php 

					// check for rows (parent repeater)
					if( have_rows('priceList__rep') ): ?>
						<?php 

						// loop through rows (parent repeater)
						while( have_rows('priceList__rep') ): the_row(); ?>
						<div class="row">
							<div class="col-md-12 accordion">
								<div class="accordion__header">
									<h3><?php the_sub_field('priceList__rep__name'); ?></h3> 
									<h4><?php the_sub_field('priceList__rep__price'); ?></h4>
								</div>
								<?php 

								// check for rows (sub repeater)
								if( have_rows('priceList__rep__rep') ): ?>
									<ul class="accordion__body">
									<?php 

									// loop through rows (sub repeater)
									while( have_rows('priceList__rep__rep') ): the_row();

										// display each item as a list - with a class of completed ( if completed )
										?>
										<li><?php the_sub_field('priceList__rep__rep__li'); ?></li>
									<?php endwhile; ?>
									</ul>
								<?php endif;  ?>
							</div>
						</div>

						<?php endwhile; ?>
					<?php endif; ?>

					<div class="btn-wrapper">
						<a href="#contacts" class="btn btn--secondary">Mám zájem</a>
					</div>
					
				</section>

				<section class="article--priceList__single">
					<p class="introText">Výživový program Metabolic Balance®</p>

					<?php 

					// check for rows (parent repeater)
					if( have_rows('metabolic__rep') ): ?>
						<?php 

						// loop through rows (parent repeater)
						while( have_rows('metabolic__rep') ): the_row(); ?>
						<div class="row">
							<div class="col-md-12 accordion">
								<div class="accordion__header">
								<h3><?php the_sub_field('metabolic__rep__name'); ?></h3> 
								<h4><?php the_sub_field('metabolic__rep__price'); ?></h4>
							</div>
								<?php 

								// check for rows (sub repeater)
								if( have_rows('metabolic__rep__rep') ): ?>
									<ul class="accordion__body">
									<?php 

									// loop through rows (sub repeater)
									while( have_rows('metabolic__rep__rep') ): the_row();

										// display each item as a list - with a class of completed ( if completed )
										?>
										<li><?php the_sub_field('metabolic__rep__rep__li'); ?></li>
									<?php endwhile; ?>
									</ul>
								<?php endif;  ?>
							</div>	
						</div>

						<?php endwhile; ?>
					<?php endif; ?>

					<div class="btn-wrapper">
						<a href="#contacts" class="btn btn--secondary">Mám zájem</a>
					</div>

				</section>

				<section class="article--priceList__single">
					<p class="introText">Životní koučink a osobní rozvoj</p>
				
					<?php 

					// check for rows (parent repeater)
					if( have_rows('coaching__rep') ): ?>
						<?php 

						// loop through rows (parent repeater)
						while( have_rows('coaching__rep') ): the_row(); ?>
						<div class="row">
							<div class="col-md-12 accordion">
								<div class="accordion__header">
								<h3><?php the_sub_field('coaching__rep__name'); ?></h3> 
								<h4><?php the_sub_field('coaching__rep__price'); ?></h4>
							</div>
								<?php 

								// check for rows (sub repeater)
								if( have_rows('coaching__rep__rep') ): ?>
									<ul class="accordion__body">
									<?php 

									// loop through rows (sub repeater)
									while( have_rows('coaching__rep__rep') ): the_row();

										// display each item as a list - with a class of completed ( if completed )
										?>
										<li><?php the_sub_field('coaching__rep__rep__li'); ?></li>
									<?php endwhile; ?>
									</ul>
								<?php endif;  ?>
							</div>
						</div>

						<?php endwhile; ?>
					<?php endif; ?>

					<div class="btn-wrapper">
						<a href="#contacts" class="btn btn--secondary">Mám zájem</a>
					</div>

				</section>

				<section class="article--priceList__single">
					<p class="introText">Terapie pomocí Bachových esencí</p>

					<?php 

					// check for rows (parent repeater)
					if( have_rows('theraphy__rep') ): ?>
						<?php 

						// loop through rows (parent repeater)
						while( have_rows('theraphy__rep') ): the_row(); ?>
						<div class="row">
							<div class="col-md-12 accordion">
								<div class="accordion__header">
								<h3><?php the_sub_field('theraphy__rep__name'); ?></h3> 
								<h4><?php the_sub_field('theraphy__rep__price'); ?></h4>
							</div>
								<?php 

								// check for rows (sub repeater)
								if( have_rows('theraphy__rep__rep') ): ?>
									<ul class="accordion__body">
									<?php 

									// loop through rows (sub repeater)
									while( have_rows('theraphy__rep__rep') ): the_row();

										// display each item as a list - with a class of completed ( if completed )
										?>
										<li><?php the_sub_field('theraphy__rep__rep__li'); ?></li>
									<?php endwhile; ?>
									</ul>
								<?php endif;  ?>
							</div>
						</div>

						<?php endwhile; ?>
					<?php endif; ?>

					<div class="btn-wrapper">
						<a href="#contacts" class="btn btn--secondary">Mám zájem</a>
					</div>

				</section>

				<section class="article--priceList__single">
					<p class="introText">Online produkty</p>

					<?php 

					// check for rows (parent repeater)
					if( have_rows('online__rep') ): ?>
						<?php 

						// loop through rows (parent repeater)
						while( have_rows('online__rep') ): the_row(); ?>
						<div class="row">
							<div class="col-md-12 accordion">
								<div class="accordion__header">
								<h3><?php the_sub_field('online__rep__name'); ?></h3> 
								<h4><?php the_sub_field('online__rep__price'); ?></h4>
							</div>
								<?php 

								// check for rows (sub repeater)
								if( have_rows('online__rep__rep') ): ?>
									<ul class="accordion__body">
									<?php 

									// loop through rows (sub repeater)
									while( have_rows('online__rep__rep') ): the_row();

										// display each item as a list - with a class of completed ( if completed )
										?>
										<li><?php the_sub_field('online__rep__rep__li'); ?></li>
									<?php endwhile; ?>
									</ul>
								<?php endif;  ?>
							</div>
						</div>

						<?php endwhile; ?>
					<?php endif; ?>

					<div class="btn-wrapper">
						<a href="#contacts" class="btn btn--secondary">Mám zájem</a>
					</div>

				</section>

			</div>
		</div>
		<!-- / -->

		<!-- Offer -->
		<section class="section section--offer">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="section--offer__content">
							<h2>Sestavím pro Vás individuální balíček služeb na míru Vašim potřebám a osobním cílům</h2>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- / -->

		<!-- CTA -->
		<section class="section section--cta">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="section--cta__content">
							<h2>Darujte zdraví</h2>
							<p>Na své služby Vám ráda vystavím dárkový poukaz</p>
							<div class="btn-wrapper">
								<a class="btn btn--secondary" href="#">Mám zájem</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- / -->

	</article>
	<!-- / -->

<?php get_footer(); ?>