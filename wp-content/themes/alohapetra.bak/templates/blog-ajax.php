<link rel="stylesheet" href="http://www.alohapetra.cz/wp-content/themes/alohapetra/css/main.css?ver=<?= version_css ?>" media="all">
<script>
(function($){   

   $('#resetFilter').click(function(e){
        $('#paged').val('');
        resetForm($('#filter'));
        window.location.reload();
        //$.fn.filterRecipe();
        
   });
   
	/*$('.category__filter').change(function(){
        $.fn.filterRecipe();
        return false;
	}); */
    
 /*$.fn.filterRecipe = function(){
		var filter = $('#filter');
		$.ajax({
			url:filter.attr('action'),
			data:filter.serialize(), // form data
			type:filter.attr('method'), // POST
			beforeSend:function(xhr){
				//filter.find('button').text('Hledám...'); // changing the button label
			},
			success:function(data){
				//filter.find('button').text('Filtrováno'); // changing the button label back
				$('.boxes--start').html(data); // insert data
			}
		});
		return false;
}*/

	$('.page-numbers').on('click',function(){
        var pageNum = '';
        var strana = 0;
        pageNum  = $(this).attr('href').match(/\d+/)[0];    
        $('#paged').val(pageNum);
        $.fn.filterRecipe();
        return false;
    }); 

})(jQuery);

function resetForm($form) {
    $form.find('input:text, input:password, input:file, select, textarea').val('');
    $form.find('input:radio, input:checkbox')
         .removeAttr('checked').removeAttr('selected');
}
</script>
<?
$category = $_POST['category'];
$materials = $_POST['materials'];
$page = $_POST['paged'];
$paged = ( $page ) ? $page : 1;
$search = $_POST['search'];
$sort = $_POST['sorted'];

$sortArgs = [
    1 => [
        'publish_date',
        'DESC',                
    ],
    2 => [
        'publish_date',
        'ASC',
    ],
    3 => [
        'title',
        'ASC'
    ],
    4 => [
        'title',
        'DESC'
    ]
];

if (isset($sortArgs[$sort])){
    $orderby = $sortArgs[$sort][0];
    $order =  $sortArgs[$sort][1];
}
else{
    $orderby = 'publish_date';
    $order =  'DESC';
}

$argsFilter = [
    'post_type' => 'post',
    'posts_per_page' => 10,
    'paged' => $paged,
    'orderby'=> $orderby,
    'order' => $order     
];

if (is_array($category) && count($category) > 0){
    $filterDataCategory = implode(',',$category);
    $argsFilter['cat'] = '"'.$filterDataCategory.'"';
}

if (is_array($materials) && count($materials) > 0){
    $filterDataMaterials = implode(',',$materials);
    $tags = '"'.$filterDataMaterials.'"';
    $argsFilter['tax_query'] = array(
        array( 'taxonomy' => 'recepty-tags', 'field' => 'term_id', 'terms' => $materials )
    );
}

if($search != ''){
    $argsFilter['s'] = $search;
}


?>

<?php $catquery = new WP_Query( $argsFilter ); ?>
<?php while($catquery->have_posts()) : $catquery->the_post(); ?>
<?php $is_premium = pms_is_post_restricted( get_the_ID() ) ? 'is-premium'  : FALSE;?>
<div class="box box--2 <?= $is_premium ?>">
<a class="box__link" href="<?php echo get_permalink();?>"></a>
<div class="box__header">
<?php the_post_thumbnail();?>
</div>
<div class="box__body">
<h3><?php echo get_the_title();?></h3>
</div>
<div class="box__premium">
<img src="<?php echo get_template_directory_uri(); ?>/img/icon-locked.svg" alt="Premium recept">
<h4>Premium recept</h4>
<a href="<?php the_permalink();?>">Zakoupit členství</a>
</div>
</div>
<?php wp_reset_postdata(); ?>
<?php endwhile; ?>                       	 	
<div class="col-md-12 box__pagination">
<?php
echo paginate_links( array(
        'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
        'total'        => $catquery->max_num_pages,
        'current'      => max( 1, $page ),
        'format'       => '?paged=%#%',
        'show_all'     => false,
        'type'         => 'plain',

    ) );
?>
</div>