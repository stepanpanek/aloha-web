﻿<?php /* Template Name: About */ ?>

<?php get_header(); ?>

	<!-- Article -->	
	<article class="article article--about">

		<!-- Header -->
		<header class="article__header lazy" data-bg="<?php echo get_the_post_thumbnail_url();?>">
			
			<ul class="fastContact">
				<li><a href="mailto:info@zoom-cargo.cz"><img class="lazy" data-src="<?php echo get_template_directory_uri(); ?>/img/icon-email.svg" alt=""> <span>info@zoom-cargo.cz</span></a></li>
				<li><a href="tel:+420603195250"><img class="lazy" data-src="<?php echo get_template_directory_uri(); ?>/img/icon-tel.svg" alt=""> tel. <span>+420 603 195 250</span></a></li>
			</ul>

			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="box">
							<div class="box__header">
								<h1><?php the_title();?></h1>
							</div>
							<div class="box__body">
                                <p>Jsme tým , který svou práci miluje. Jsme spolu dlouho (rádi) a v oboru jsme dlouho. Přesto na sobě stále pracujeme, abychom pro vás zajišťovali, co nejlepší služby.</p>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- Stripe -->
			<div class="article__header__stripe">
				<span id="scrollDown">
					<img class="lazy" data-src="<?php echo get_template_directory_uri();?>/img/arrow-bottom.svg" alt="Šipka dolů">
				</span>
				<div class="container">
					<div class="row">
						<div class="offset-md-8 col-md-4">
							<h2>Zavolejte nám, nebo napište</h2>
						</div>
					</div>
				</div>	
			</div>
			<!-- / -->

		</header>
		<!-- / -->
		
		<!-- Content -->
		<div class="article__content">

			<div class="article__content__info">
				<div class="container">
					<div class="row">
						<div class="col-lg-8">
							<?php the_field('service__desc');?>
						</div>
						<div class="col-lg-4 boxes">

							<div class="box box--1">
								<div class="box__header">
									<img class="lazy" data-src="<?php echo get_template_directory_uri(); ?>/img/icon-tel.svg" alt="Ikona telefonu">
								</div>
								<div class="box__body">
									<p>Zavolejte nám</p>
									<a href="tel:+420603195250">tel. +420 603 195 250</a>
								</div>	
                            </div>

							<div class="box box--1">
								<div class="box__header">
									<img class="lazy" data-src="<?php echo get_template_directory_uri(); ?>/img/icon-tel.svg" alt="Ikona telefonu">
								</div>
								<div class="box__body">
									<p>Zavoláme vám zpět</p>
									<a href="tel:+420603195250">nechte nám své tel. číslo</a>
								</div>
							</div>

							<div class="box box--1">
								<div class="box__header">
									<img class="lazy" data-src="<?php echo get_template_directory_uri(); ?>/img/icon-email.svg" alt="Ikona e-mailu">
								</div>
								<div class="box__body">
									<p>Pokud preferujete e-mail</p>
									<a href="mailto:info@zoom-cargo.com">info@zoom-cargo.com</a>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>

			<div class="article__content__boxes">
				<div class="container">
					<div class="row">

					    <?php if( have_rows('service__rep') ): ?>

						<div class="col-md-12 boxes boxes--start">

						<?php while( have_rows('service__rep') ): the_row(); 

							// vars
							$servImg = get_sub_field('service__img');
							$servTitle = get_sub_field('service__title');
							$servDesc = get_sub_field('service__desc');

							?>

							<div class="box box--3">
								<div class="box__header">
									<h3><?php echo $servTitle; ?></h3>
								</div>
								<div class="box__img"> 
									<?php
										$image = get_sub_field('service__img', false);
										$size = 'gallery';

										echo wp_get_attachment_image( $image, $size, false, array(
											'src' => '',
											'srcset' => '',
											'class' => 'lazy',
											'data-src' => wp_get_attachment_image_url( $image, $size ),
										) );
                                    ?>
								</div>
								<div class="box__body">
									<?php echo $servDesc; ?>
									<div class="btn-wrapper">
										<a href="#form" class="btn btn--primary">Objednat přepravu</a>
									</div>
								</div>
							</div>

						<?php endwhile; ?>

						</div>

					<?php endif; ?>

                    </div>
                    <h2>Naši partneři</h2>
					<div class="row">

					    <?php if( have_rows('service__rep2') ): ?>

						<div class="col-md-12 boxes boxes--start">

						<?php while( have_rows('service__rep2') ): the_row(); 

							// vars
							$servImg2 = get_sub_field('service__img2');
							$servTitle2 = get_sub_field('service__title2');
							$servDesc2 = get_sub_field('service__desc2');

							?>

							<div class="box box--3">
								<div class="box__header">
									<h3><?php echo $servTitle2; ?></h3>
								</div>
								<div class="box__img"> 
									<?php
										$image2 = get_sub_field('service__img2', false);
										$size2 = 'gallery';

										echo wp_get_attachment_image( $image2, $size2, false, array(
											'src' => '',
											'srcset' => '',
											'class' => 'lazy',
											'data-src' => wp_get_attachment_image_url( $image2, $size2 ),
										) );
                                    ?>
								</div>
								<div class="box__body">
									<?php echo $servDesc2; ?>
								</div>
							</div>

						<?php endwhile; ?>

						</div>

					<?php endif; ?>

					</div>
				</div>
			</div>

		</div>
		<!-- / -->

		<!-- Contact form -->
		<section id="form" class="section--contact">
			<div class="container">
				<h2>Potřebujete přepravit zásilku?</h2>
				<div class="row">
					<div class="col-md-12">
						<?php echo do_shortcode( '[contact-form-7 id="86" title="Objednavka"]'); ?>
					</div>
				</div>
			</div>
		</section>
		<!-- / -->

	</article>
	<!-- / -->

<?php get_footer(); ?>