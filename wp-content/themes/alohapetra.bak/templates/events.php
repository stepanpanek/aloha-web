﻿<?php /* Template Name: Events */ ?>

<?php get_header(); ?>

	<!-- Article -->	
	<article class="article article--events">

		<!-- Header -->
		<header class="article__header lazy">
			
		    <!-- Hello -->
			<section id="hello" class="section section--hello">
				<div class="container">
					<div class="row">
						<div class="col-md-8">
							<h1><?php the_field('hello__title');?></h1>
							<a class="scrollDown" href="#content">
								<img class="lazy" data-src="<?php echo get_template_directory_uri();?>/img/arrow-down.svg" alt="Šipka dolů">
							</a>
						</div>
					</div>
				</div>
				<div class="section--hello__bg">
					<img class="lazy" data-src="<?php the_field('hello__img');?>" alt="Aloha Petra úvodní obrázek">
				</div>
			</section>
			<!-- / -->

		</header>
		<!-- / -->
		
		<!-- Content -->
		<div id="content" class="article__content">
			<div class="container">
				<div class="row">

					<div class="col-md-12 boxes">

						<?php  	$catquery = new WP_Query( array(
								'post_type'    => 'product',
								'post__not_in' => array( 363, ),
							) ); ?>

						<?php while($catquery->have_posts()) : $catquery->the_post(); ?>

							<div class="box box--1">
								<a class="box__link" href="<?php echo get_permalink();?>"></a>
								<div class="box__header">
									<?php the_post_thumbnail();?>
								</div>
								<div class="box__body">
									<div class="box__body--up">
										<p class="introText">
											<?php the_field('product__date');?> / <?php the_field('product__city');?>
										</p>
										<span>
											<?php
											$terms = get_the_terms($product->get_id(), 'product_cat');
												foreach ($terms as $term) {

													$product_cat = $term->name;
													echo $product_cat;
														break;
											}
											?>
										</span>
									</div>
									<div class="body__body--down">
										<h2><?php the_title();?></h2>
										<p><?php the_field('product__short');?></p>
										<div class="btn-wrapper">
											<a href="<?php echo get_permalink();?> " class="btn btn--primary"><span>Chci vědět více</span></a>
										</div>
									</div>
								</div>
							</div>
							
						<?php endwhile; ?>

					</div>
				</div>
			</div>
		</div>
		<!-- / -->

		<!-- CTA -->
		<section class="section section--cta">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="section--cta__content">
							<h2>Text - ráda uspořádám akci i u vás</h2>
							<p>Ilustrační text - máte zájem o uspořádání akce?</p>
							<div class="btn-wrapper">
								<a class="btn btn--secondary" href="#">Mám zájem</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- / -->

	</article>
	<!-- / -->

<?php get_footer(); ?>