﻿<?php get_header() ?>

	<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : the_post() ?>

	<!-- Article -->	
	<article class="article article--singleRecipe">

		<!-- Header -->
		<header class="article__header">
			
			<div class="article__header__intro">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<a href="<?php echo get_site_url();?>/recepty" id="back">
								<img class="lazy" data-src="<?php echo get_template_directory_uri();?>/img/arrow-left.svg" alt="Šipka zpět">
								<span>Zpět na filtr</span>
							</a>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<p class="introText">Mé recepty</p>
							<h1><?php the_title(); ?></h1>
							
								<ul>    
								<?php
                                /* show category without parent category */ 
                                $catID=$wp_query->query_vars['cat'];
								$args = array('parent' => $catID);
								$categories = get_categories( $args );
								foreach($categories as $category) {
                                    if($category->parent == 0) continue; 
									echo '<li><a href="' . get_category_link( $category->term_id ) . '"  ' . '>' . $category->name.'</a> </li> ';
								}
                                
                                ?></ul><ul>
                                <?php
                                $tags = get_terms(array('taxonomy' => 'recepty-tags'));
                                foreach($tags as $tag){
                                    echo '<li><a href="'.get_term_link($tag->term_id).'">'.$tag->name.'</a></li>';
                                }

									?>
                                    
								</ul>

						</div>
					</div>
				</div>
			</div>

			<div class="article__header__img">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<img src="<?php echo get_the_post_thumbnail_url();?>" alt="Fotka">
						</div>
					</div>
				</div>
			</div>

		</header>
		<!-- / -->
		
		<!-- Content -->
		<div class="article__content">
			<div class="container">
				<div class="row">
					<div class="col-md-12">

						<!-- Info -->
						<div class="article--singleRecipe__info block">
							<h2 class="introText">Úvod</h2>
							<?php the_field('intro__text'); ?>
						</div>
						<!-- / -->

						<!-- Ingredients -->
						<div class="article--singleRecipe__ingredients block">
							<h2 class="introText">Suroviny pro <?php the_field('ingredients__number'); ?></h2>

							<?php if( have_rows('ingredients__rep') ): ?>
							<ul>
							<?php while( have_rows('ingredients__rep') ): the_row(); 

								// vars
								$ingText = get_sub_field('ingredients__text');

								?>

								<li><?php echo $ingText; ?></li>

							<?php endwhile; ?>
							</ul>
							<?php endif; ?>

						</div>
						<!-- / -->

						<!-- Process -->
						<div class="article--singleRecipe__process block">
							<h2 class="introText">Postup</h2>

							<?php if( have_rows('process__rep') ): ?>
							<ol>
							<?php while( have_rows('process__rep') ): the_row(); 

								// vars
								$processText = get_sub_field('process__text');

								?>

								<li><?php echo $processText; ?></li>

							<?php endwhile; ?>
							</ol>
							<?php endif; ?>
							
						</div>
						<!-- / -->

						<!-- Recommendation -->
						<div class="article--singleRecipe__recommendation block">
							<h2>Tip</h2>
							<?php the_field('recommendation__text'); ?>
						</div>
						<!-- / -->

					</div>
				</div>
			</div>
		</div>
		<!-- / -->

        <!-- Gallery -->
        <section id="gallery" class="section section--gallery">
            <div class="container">
				<!-- Swiper -->
				<div class="swiper-container-recipe">
					<div class="swiper-wrapper chocolat-parent">
					<?php if( have_rows('gallery__rep') ): ?>

					<?php while( have_rows('gallery__rep') ): the_row(); 

						// vars
						$galText = get_sub_field('gallery__rep__text');
						$galImg = get_sub_field('gallery__rep__img');

						?>                           
						<figure class="swiper-slide">
							<a class="chocolat-image" href="<?php echo $galImg; ?>" data-toggle="lightbox">
								<div class="swiper-slide_wrapper">
									<img src="<?php echo $galImg; ?>" alt="<?php echo $galText; ?>">
								</div>
								<figcaption><?php echo $galText; ?></figcaption>
							</a>
						</figure>

					<?php endwhile; ?>

					<?php endif; ?>

					</div>
					<nav class="swiper-nav">
						<div class="prev">
							<img class="lazy" data-src="<?php echo get_template_directory_uri();?>/img/arrow-left-blue.svg">
						</div>
						<div class="next">
							<img class="lazy" data-src="<?php echo get_template_directory_uri();?>/img/arrow-right-blue.svg">
						</div>
					</nav>
				</div>
				<!-- / -->
            </div>
        </section>
		<!-- / -->
		
		<!-- Footer -->
		<div class="article__footer">
			<div class="container">				
				
			<div id="red" class="row">

					<div class="col-6">
						<a id="print" href="javascript:window.print()">
							<img class="lazy" data-src="<?php echo get_template_directory_uri();?>/img/icon-print.svg" alt="Ikona pro tisk">
							<span>Tisk receptu</span>
						</a>
					</div>
					<div class="col-6">
						<p class="article__date"><?php echo get_the_date();?></p>
					</div>
				</div>

				<!-- CTA -->
				<section class="section section--cta is-overlayed">
					<div class="row">
						<div class="col-md-12">
							<div class="section--cta__content">
								<h2>Zde bude nějaký krátký text ohledně možnosti zakoupení premium sekce.</h2>
								<div class="btn-wrapper">
									<a class="btn btn--secondary" href="#">Chci premium členství</a>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- / -->

			</div>
		</div>
		<!-- / -->

		<!-- Share it -->
		<div class="pageShare">
			<div class="pageShare__header">
				<span id="showShareIcons">
					<img class="lazy" data-src="<?php echo get_template_directory_uri();?>/img/share.svg" alt="Ikona pro sdílení">
				</span>	
			</div>
			<div class="pageShare__body">
				<ul>
					<li>
						<a href="https://twitter.com/share?url=<?php echo get_site_url();?>" target="_blank">
							<span>Sdílet na Twitteru</span>
							<img class="lazy" data-src="<?php echo get_template_directory_uri();?>/img/icon-share-fb.svg" alt="Sdílet na Twitteru">
						</a>
					</li>
					<li>
						<a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo get_site_url();?>" target="_blank">
							<span>Sdílet na LinkedIn</span>
							<img class="lazy" data-src="<?php echo get_template_directory_uri();?>/img/icon-share-linkedin.svg" alt="Sdílet přes LinkedIn">
						</a>
					</li>
					<li>
						<a href="http://www.facebook.com/sharer.php?u=<?php echo get_site_url();?>" target="_blank">
							<span>Sdílet na Facebooku</span>
							<img class="lazy" data-src="<?php echo get_template_directory_uri();?>/img/icon-share-fb.svg" alt="Sdílet přes FB">
						</a>
					</li>
				</ul>
			</div>
		</div>
		<!-- / -->

	</article>
	<!-- / -->

		<?php endwhile ?>
	<?php endif ?>

<?php get_footer() ?>