<?php get_header() ?>

	<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : the_post() ?>

			<!-- Article -->
			<article <?php post_class() ?>>
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1 class="post-title"><?php the_title() ?></h1>
							<?php the_content('') ?>
						</div>
					</div>
				</div>
			</article>
			<!-- / -->

		<?php endwhile ?>
	<?php endif ?>

<?php get_footer() ?>