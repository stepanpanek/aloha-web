<?php

/**
 * Add scripts & styles
 */
add_filter( 'wpcf7_load_css', '__return_false' );
define( 'version_css', '7.5.5' );

add_action('wp_enqueue_scripts', 'muzli_theme_scripts');
function muzli_theme_scripts()
{


    wp_enqueue_script(
		'aloha-lazy', get_template_directory_uri() . '/js/lazyload.min.js',
		array( 'jquery' ), '', true
	);	

	wp_enqueue_script(
		'aloha-swiper', get_template_directory_uri() . '/js/swiper-bundle.min.js',
		array( 'jquery' ), '', true
	);

	wp_enqueue_script(
		'aloha-gallery', get_template_directory_uri() . '/js/chocolat.min.js',
		array( 'jquery' ), '', true
	);

	wp_enqueue_style(
		'aloha-maincss', get_template_directory_uri() . '/css/main.css',
		array(), version_css, 'all'
	);	

	wp_enqueue_script(
		'aloha-mainjs', get_template_directory_uri() . '/js/main.js',
		array( 'jquery' ), '7.5.5', true
	);
}
