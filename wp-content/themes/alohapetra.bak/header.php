<!DOCTYPE html>
<html <?php language_attributes() ?>>
<head>
	<meta charset="<?php bloginfo('charset') ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="alternate" type="application/rss+xml" title="<?php echo get_bloginfo('sitename') ?> Feed" href="<?php echo get_bloginfo('rss2_url') ?>">
    <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Playfair+Display:wght@400;500;600&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700&display=swap" rel="stylesheet">
    <?php wp_head() ?>  
</head>

<body <?php body_class(); ?>>
    
    <!-- Site header -->
	<header id="header" class="header header--main" role="navigation">
        <div class="container container--large">
            <nav class="navigation navigation--main">

                <!-- Site brand -->
                <a class="navigation--main__brand" href="<?php echo site_url(); ?>">
                </a>
                <!-- / -->

                <!-- Menu items -->
                <div id="mainMenu" class="navigation--main__menu">
                    <?php wp_nav_menu( array(
                    'theme_location'  => 'primary',
                    'container'       => false,
                    ) ) ?>
                </div>
                <!-- / -->

                <!-- Menu toggle -->
                <button id="toggle" class="navigation--main__toggle" type="button">
                    <span></span>
                    <span></span>
                </button>
                <!-- / -->

            </nav>
        </div>
    </header>
    <!-- / -->

    <!-- Main content -->
    <main id="main" role="main">