<?php /* Template Name: CZ - Aktivity */ ?>

<?php get_header() ?>

	<!-- Article -->
	<article class="article article--default article--activities">
		
		<!-- Header of article -->
		<header class="article__header">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h1>Naše články</h1>
					</div>
				</div>
			</div>
		</header>
		<!-- / -->
		
		<!-- Activities -->
		<section class="section section--posts">
			<div class="container">
				<div class="row">
					<div class="col-md-12 boxes">
                        
                        <?php $catquery = new WP_Query( 'posts_per_page=-1' ); ?>

                        <?php while($catquery->have_posts()) : $catquery->the_post(); ?>

                            <div class="box box--1">
                                <div class="box__header">
                                    <h4><?php echo wp_trim_words( get_the_title(), 10 ); ?></h4>
                                    <?php the_excerpt(); ?>
                                    <a class="btn btn--secondary" href="<?php the_permalink() ?>">Pokračovat ve čtení</a>
                                </div>
                                <div class="box__body">
                                    <?php the_post_thumbnail( 'large' ); ?>
                                </div>
                            </div>
                            
                        <?php endwhile; ?> 	

					</div>
				</div>
			</div>
		</section>
		<!-- / -->

	</article>
	<!-- / -->

<?php get_footer() ?>