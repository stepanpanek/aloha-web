<?php get_header() ?>

	<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : the_post() ?>
		
		<!-- Article -->	
		<article class="article article--singlePage">

			<!-- Header -->
			<header class="article__header">	
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1><?php the_title(); ?></h1>
						</div>
					</div>
				</div>
			</header>
			<!-- / -->
		
			<!-- Content -->
			<div class="article__content">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
			</div>
			<!-- / -->

		</article>
		<!-- / -->

		<?php endwhile ?>
	<?php endif ?>

<?php get_footer() ?>